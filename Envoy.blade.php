@servers(['web' => 'eddythedove@ps528823.dreamhostps.com'])

@task('deploy', ['on' => 'web'])
    cd taskimi.com
    git pull
    exit
@endtask

@task('migrate', ['on' => 'web'])
    cd taskimi.com
    php artisan migrate --force
    exit
@endtask
