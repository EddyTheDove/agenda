<?php

namespace App\Http\Controllers\Api\v1;

use DB;
use Auth;
use App\Models\Task;
use App\Http\Requests;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Traits\PusherTrait;

class CommentController extends Controller
{
    use PusherTrait;


    /**
     * Get comments for single taask, single user or all comments
     * @param  {[type]} Request $request      [description]
     * @return {[type]}         [description]
     */
    public function index(Request $request)
    {
        $comments = [];

        if($request->has('task') && $request->task != "")
        {
            $comments = Comment::where('task_id', $request->task)->paginate(5);
        }

        else if($request->has('user') && $request->user != "")
        {
            $comments = Comment::where('user_id', $request->user)->paginate(5);
        }

        else
        {
            $comments = Comment::orderBy('id', 'desc')->paginate(1);
        }


        //Add the author and convert the time for readability
        //We only take 5 comments. It should be alright
        if(sizeof($comments) > 0)
        {
            foreach($comments as $com)
            {
                $com->author = $com->user_id == Auth::user()->id ? trans('app.me') : $com->user->username();
                $com->date = date('d/m/Y H:i', strtotime($com->created_at));
            }
        }

        return response()->json($comments, 200);
    }


    /**
     * Save a new task
     * @param  {[type]} Request $request      [description]
     * @return {[type]}         [description]
     */
    public function store(Request $request)
    {
        try
        {
            $rules = [
                'content' => 'required',
                'task_id' => 'required'
            ];

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails())
            {
                $result = $validator->errors()->all();
                $statusCode = self::STATUSCODE_BADREQUEST;
                return response()->json($result, $statusCode);
            }

            $task = Task::find($request->task_id);
            if(!$task){
                return response()->json(trans('app.not_found'), 404);
            }

            $comment = Comment::create([
                'user_id' => Auth::user()->id,
                'task_id' => $task->id,
                'content' => $request->content
            ]);

            $comment->author = trans('app.me');
            $comment->date = date('d/m/Y H:i', strtotime($comment->created_at));

            if($task->folder->isShared()){
                $pusher = $this->getPusher();
                $pusher->trigger('task-channel', 'newcomment', []);
            }

            return response()->json($comment, 200);
        }
        catch (\Exception $e)
        {
            return response()->json($e->getMessage(), 500);
        }
    }
}
