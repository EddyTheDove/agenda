<?php

namespace App\Http\Controllers\api\v1;

use Auth;
use Session;
use Carbon\Carbon;
use App\Functions;
use App\Models\Event;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
	public function index(Request $request)
	{
		try
		{
			$user = Auth::user();

			if($request->has('limit')){
				$limit = (int) $request->limit;
				$events = Event::where('user_id', $user->id)->future()->orderBy('id', 'desc')->take($limit)->get();
			}
			elseif($request->has('calendar'))
			{
				$events = Event::where('user_id', $user->id)->future()->orderBy('start_date')->get();
			}
			else
			{
				$events = Event::where('user_id', $user->id)->future()->orderBy('start_date')->paginate(12);
			}

			if($events){
				Functions::changeDefaultLocale(Session::get('locale'));
				foreach($events as $event){
					$event->daynumber = strftime('%d', strtotime($event->start_date));
					$event->dayletter = strftime('%A', strtotime($event->start_date));
					$event->month = strftime('%B', strtotime($event->start_date));
					$event->year = strftime('%G', strtotime($event->start_date));
					$event->days = Carbon::parse($event->start_date)->diffInDays(Carbon::today());
					$event->time = $event->hour.':'.$event->minutes;
				}
			}
			$statusCode = self::STATUSCODE_SUCCESS;

			return response(compact('events'), $statusCode);
		}
		catch (\Exception $e)
		{
			$result = [ "error" => $e->getMessage()];
			$statusCode = self::STATUSCODE_ERROR;
		}

	}



	/**
	* [store description]
	* @param  Request $request [description]
	* @return [type]           [description]
	*/
	public function store(Request $request)
	{
		try
		{
			$statusCode = self::STATUSCODE_BADREQUEST;

			$rules = [
				'title'	  => 'required',
				'startdate' => 'required'
			];

			$validator = Validator::make($request->all(), $rules);

			if($validator->fails())
			{
				$result = $validator->errors()->all();
				$statusCode = self::STATUSCODE_BADREQUEST;
			}
			else
			{
				if($request->enddate != ''){
					if($request->enddate < $request->startdate){
						$result = trans('app.enddate_not_lower');
						return response(compact('result'), $statusCode);
					}
				}

				$event = new Event([
					'title'	   		=> $request->title,
					'hour'  		=> $request->hour,
					'minutes' 		=> $request->minutes,
					'description' 	=> $request->description,
					'start_date'  	=> Carbon::parse($request->startdate),
					'code'			=> $this->getCode(),
					'label'	   		=> $request->label,
					'user_id'	 	=> Auth::user()->id
				]);

				if($request->enddate != ''){
					$event->end_date  = Carbon::parse($request->enddate);
				}

				$event->save();
				$result = $event;
				$statusCode = self::STATUSCODE_SUCCESS;
			}

			return response(compact('result'), $statusCode);
		}
		catch (\Exception $e)
		{
			$result = [ "error" => $e->getMessage()];
			$statusCode = self::STATUSCODE_ERROR;
		}
	}



	/**
	* [show single event]
	* @param  [type] $code [description]
	* @return [type]	   [description]
	*/
	public function show($code)
	{
		$event = Event::where('code', $code)
		->where('user_id', Auth::user()->id)
		->first();

		if(!$event)
		{
			$result = trans('app.event_not_found');
			$statusCode = self::STATUSCODE_NOTFOUND;
		}
		else
		{
			$event->daynumber 	= strftime('%d', strtotime($event->start_date));
			$event->dayletter 	= strftime('%A', strtotime($event->start_date));
			$event->month 	  	= strftime('%B', strtotime($event->start_date));
			$event->year 	  	= strftime('%G', strtotime($event->start_date));
			$event->days 	  	= Carbon::parse($event->start_date)->diffInDays(Carbon::today());
			$event->short_start = strftime('%d-%m-%G', strtotime($event->start_date));
			$event->time 		= $event->hour.':'.$event->minutes;

			if($event->end_date){
				$event->daynumber2 = strftime('%d', strtotime($event->end_date));
				$event->month2 	   = strftime('%B', strtotime($event->end_date));
				$event->year2 	   = strftime('%G', strtotime($event->end_date));
				$event->short_end  = strftime('%d-%m-%G', strtotime($event->end_date));
			}

			$result = $event;
			$statusCode = self::STATUSCODE_SUCCESS;
		}

		return response(compact('result'), $statusCode);
	}



	/**
	* [update description]
	* @param  [type] $code [description]
	* @return [type]       [description]
	*/
	public function update(Request $request, $id)
	{
		try
		{
			$result = [];
			$statusCode = self::STATUSCODE_NOTFOUND;

			$event = Event::find($id);
			if(!$event)
			{
				$result = trans('app.event_not_found');
				$statusCode = self::STATUSCODE_NOTFOUND;
			}
			else
			{
				$rules = [
					'title'	  	=> 'required',
					'startdate' => 'required'
				];

				$validator = Validator::make($request->all(), $rules);

				if($validator->fails())
				{
					$result = $event;
					$statusCode = self::STATUSCODE_BADREQUEST;
				}
				else
				{


					$event->description = $request->description;
					$event->hour  		= $request->hour;
					$event->minutes 	= $request->minutes;
					$event->start_date  = Carbon::parse($request->startdate);
					$event->title 		= $request->title;
					$event->label 		= $request->label;

					if($request->enddate != ''){
						$end = Carbon::parse($request->enddate);
						if($end < $event->start_date){
							$result = trans('app.enddate_not_lower');
							return response(compact('result'), $statusCode);
						}
						$event->end_date = $end;
					}

					$event->save();
					$result = $event;
					$statusCode = self::STATUSCODE_SUCCESS;
				}
			}

			return response(compact('result'), $statusCode);
		}

		catch (\Exception $e)
		{
			$result = [ "error" => $e->getMessage()];
			$statusCode = self::STATUSCODE_ERROR;
		}
	}



	/**
	* Create event code
	* @return [type] [description]
	*/
	private function getCode(){
		$code = 101102;
		if($last = Event::orderBy('id', 'desc')->first()){
			$code = $last->code += rand(1,5);
		}
		return $code;
	}
}
