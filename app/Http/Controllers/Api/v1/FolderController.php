<?php

namespace App\Http\Controllers\api\v1;

use DB;
use Auth;
use App\Models\Task;
use App\Models\Shared;
use App\Models\Folder;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FolderController extends Controller
{

    public function index(Request $request)
    {
        $folders = Folder::where('user_id', Auth::user()->id)->paginate(100);
        return response(compact('folders'), self::STATUSCODE_SUCCESS);
    }



    /**
     * [store description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        try
        {
            $result = [];
            $statusCode = self::STATUSCODE_BADREQUEST;

            $rules = [
				'name'	  => 'required',
				'colour'  => 'required'
			];

			$validator = Validator::make($request->all(), $rules);

			if($validator->fails())
			{
				$result = $validator->errors()->all();
			}
            else
            {
                $code = $this->getCode();
                $folder = Folder::create([
                    'user_id'   => Auth::user()->id,
                    'name'      => $request->name,
                    'colour'    => $request->colour,
                    'code'      => $code
                ]);

                $statusCode = self::STATUSCODE_SUCCESS;
                $result = $folder;
            }
            return response(compact('folder'), $statusCode);
        }
        catch (\Exception $e)
		{
			$result = [ "error" => $e->getMessage()];
			$statusCode = self::STATUSCODE_ERROR;
		}
    }



    public function update(Request $request, $id)
    {
        try
        {
            $result = [];
            $statusCode = self::STATUSCODE_NOTFOUND;

            $rules = [
				'name'	  => 'required',
				'colour'  => 'required'
			];

			$validator = Validator::make($request->all(), $rules);

			if($validator->fails())
			{
				$result = $validator->errors()->all();
			}
            else
            {
                $folder = Folder::find($id);
                if(!$folder){
                    $result = trans('app.not_found');
                }
                else
                {
                    $folder->name   = $request->name;
                    $folder->colour = $request->colour;
                    $folder->update();

                    $statusCode = self::STATUSCODE_SUCCESS;
                    $result = $folder;
                }

            }
            return response(compact('folder'), $statusCode);
        }
        catch (\Exception $e)
		{
			$result = [ "error" => $e->getMessage()];
			$statusCode = self::STATUSCODE_ERROR;
		}
    }




    /**
     * [show description]
     * @param  [type] $code [description]
     * @return [type]       [description]
     */
    public function show($code)
    {
        $folder = Folder::where('code', $code)
        ->first();

        if(!$folder){
            return response(trans('app.not_found'), self::STATUSCODE_NOTFOUND);
        }

        if(!$canAccess = $this->canAccess($folder))
        return response(trans('app.not_found'), self::STATUSCODE_NOTFOUND);

        $users = DB::table('users')
        ->join('shared_folders', 'shared_folders.user_id', '=', 'users.id')
        ->where('shared_folders.folder_id', '=', $folder->id)
        ->where('shared_folders.status', '=', 'accepted')
        ->select('users.email', 'shared_folders.folder_id as folder')
        ->get();

        return response(compact('folder', 'users'), self::STATUSCODE_SUCCESS);
    }


    /**
     * [getCode description]
     * @return [type] [description]
     */
    private function getCode()
    {
        $code = 101102;
        if($last = Folder::orderBy('id', 'desc')->first()){
            $code = $last->code += rand(1,5);
        }
        return $code;
    }


    public function destroy($id)
    {
        try {
            $folder = Folder::where('user_id', Auth::user()->id)
            ->where('id', $id)
            ->first();

            if(!$folder)
            return response()->json(trans('app.not_found'), 404);

            DB::table('tasks')
            ->where('folder_id', $folder->id)
            ->delete();

            $folder->delete();

            return response()->json(trans('app.folder_deleted'), 200);
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }


    /**
     * [canAccess description]
     * @param  [type] $folder [description]
     * @return [type]         [description]
     */
    private function canAccess($folder)
    {
        $canAccess = false;

        if($folder->user_id == Auth::user()->id)
        $canAccess = true;

        elseif(
            Shared::where('folder_id', $folder->id)
            ->where('status', 'accepted')
            ->where('user_id', Auth::user()->id)
            ->first()
        ) $canAccess = true;

        return $canAccess;
    }
}
