<?php

namespace App\Http\Controllers\Api\v1;

use Auth;
use App\Models\Task;
use App\Models\Event;
use App\Models\Folder;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        try
        {
            $result = [];

            $user = Auth::user();
            $result['events']  = Event::where('user_id', $user->id)->future()->count();
            $result['tasks']   = Task::where('user_id', $user->id)->active()->count();
            $result['folders'] = Folder::where('user_id', $user->id)->count();

            $statusCode = self::STATUSCODE_SUCCESS;

            return response(compact('result'), $statusCode);
        }
        catch (\Exception $e)
        {
            $result = [ "error" => $e->getMessage()];
            $statusCode = self::STATUSCODE_ERROR;
        }
    }
}
