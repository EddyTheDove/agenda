<?php

namespace App\Http\Controllers\Api\v1;

use DB;
use Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Shared;
use App\Models\Folder;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ShareController extends Controller
{
    public function index()
    {
        try
        {
            $user = Auth::user();
            $folders = Folder::where('user_id', $user->id)->paginate('100');
            $shares = Folder::whereHas('shares', function($query) use ($user){
                $query->where('user_id', '=', $user->id)
                ->where('status', '=', 'accepted');
            })
            ->paginate('100');

            return response()->json(compact('folders', 'shares'), 200);
        }
        catch (\Exception $e)
		{
			$result = [ "error" => $e->getMessage()];
			$statusCode = self::STATUSCODE_ERROR;
		}
    }


    public function store(Request $request)
    {
        try
        {
            $result = [];
            $statusCode = self::STATUSCODE_BADREQUEST;

            $rules = [
				'email'	 => 'required',
                'folder' => 'required'
			];

			$validator = Validator::make($request->all(), $rules);

			if($validator->fails())
			{
				$result = $validator->errors()->all();
			}
            else
            {
                $user = User::where('email', $request->email)
                ->where('id', '!=', Auth::user()->id)
                ->first();

                if(!$user){
                    $statusCode = self::STATUSCODE_NOTFOUND;
                    $result = trans('app.user_doesnt_exist');
                }
                else
                {
                    $folder = Folder::find($request->folder);

                    if(!$folder)
                    {
                        $statusCode = self::STATUSCODE_NOTFOUND;
                        $result = trans('app.folder_doesnt_exists');
                    }
                    else
                    {
                        //Is the user authorised?
                        $canShare = $this->isAuthorised($user, $folder);

                        if(!$canShare)
                        {
                            $statusCode = self::STATUSCODE_UNAUTHORISED;
                            $result = trans('app.unauthorised');
                        }
                        else
                        {

                            if($shared = $this->isShared($user, $folder))
                            {
                                if($shared->status == 'accepted')
                                {
                                    $statusCode = self::STATUSCODE_BADREQUEST;
                                    $result = trans('app.folder_user_exist');
                                }
                                else
                                {
                                    $shared->status = 'accepted';
                                    $shared->update();

                                    $statusCode = self::STATUSCODE_SUCCESS;
                                    $result = trans('app.user_added');
                                }
                            }
                            else
                            {
                                Shared::create([
                                    'folder_id'  => $folder->id,
                                    'user_id'    => $user->id,
                                ]);

                                $statusCode = self::STATUSCODE_SUCCESS;
                                $result = trans('app.user_added');
                            }
                        }
                    }
                }
            }
            return response(compact('result'), $statusCode);
        }
        catch (\Exception $e)
		{
			$result = [ "error" => $e->getMessage()];
			$statusCode = self::STATUSCODE_ERROR;
		}
    }



    /**
     * Return a shared folder
     * @param  [type]  $user   [description]
     * @param  [type]  $folder [description]
     * @return boolean         [description]
     */
    private function isShared($user, $folder)
    {
        return Shared::where('user_id', $user->id)
        ->where('folder_id', $folder->id)
        ->first();
    }



    /**
     * If the folder exist, check if we have the right to share it
     *The owner may share
     *Any user already on the folder may share
     * @param  [type] $user   [description]
     * @param  [type] $folder [description]
     * @return [type]         [description]
     */
    private function isAuthorised($user, $folder)
    {
        $canShare = false;
        if($folder->user_id = Auth::user()->id)
        {
            $canShare = true;
        }
        elseif
        (
            Shared::where('user_id', Auth::user()->id)
            ->where('folder_id', $folder->id)
            ->first()
        )
        {
            $canShare = true;
        }

        return $canShare;
    }


    /**
     * Update function
     * This function 'unshare' a folder
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $id)
    {

        try
        {
            $user = User::where('email', $id)->first();

            if(!$user)
            {
                $statusCode = self::STATUSCODE_NOTFOUND;
                $result = trans('app.user_doesnt_exist');
            }
            else
            {
                $rules = [
                    'folder' => 'required'
                ];
                $validator = Validator::make($request->all(), $rules);

    			if($validator->fails())
    			{
    				$result = $validator->errors()->all();
    			}
                else
                {
                    $folder = Shared::where('user_id', '=', $user->id)
                    ->where('folder_id', $request->folder)
                    ->where('status', 'accepted')
                    ->first();

                    if(!$folder)
                    {
                        $statusCode = self::STATUSCODE_NOTFOUND;
                        $result = trans('app.folder_doesnt_exists');
                    }
                    else
                    {
                        $folder->status = 'revoked';
                        $folder->update();

                        $statusCode = self::STATUSCODE_SUCCESS;
                        $result = trans('app.user_deleted');
                    }
                }
            }
            return response(compact('result'), $statusCode);
        }

        catch (\Exception $e)
		{
			$result = [ "error" => $e->getMessage()];
			$statusCode = self::STATUSCODE_ERROR;
		}
    }


    public function destroy($id)
    {
        try
        {
            $shared = Shared::where('user_id', Auth::user()->id)
            ->where('folder_id', $id)
            ->first();

            if(!$shared)
            return response()->json(trans('app.not_found'), 404);

            $shared->status = 'revoked';
            $shared->update();

            return response()->json(trans('app.folder_deleted'), 200);
        }
        catch (\Exception $e)
		{
            return response()->json($e->getMessage(), 500);
		}
    }
}
