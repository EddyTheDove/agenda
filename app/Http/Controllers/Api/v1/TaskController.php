<?php

namespace App\Http\Controllers\api\v1;

use DB;
use Auth;
use App\Models\Task;
use App\Models\Shared;
use App\Models\Folder;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Traits\PusherTrait;

class TaskController extends Controller
{
    use PusherTrait;

    /**
     * Get a list of tasks
     * If a folder is specified, get tasks from that folder only
     * Else, get all incomplete tasks from the logged in user
     * @param  {[type]} Request $request      [description]
     * @return {[type]}         [description]
     */
    public function index(Request $request)
    {
        if($request->has('folder') && $request->folder != ""){
            $canAccess = false;
            $currentFolder = Folder::where('code', $request->folder)->first();
            if($currentFolder->user_id == Auth::user()->id) $canAccess = true;
            elseif(
            Shared::where('folder_id', $currentFolder->id)
            ->where('status', 'accepted')
            ->where('user_id', Auth::user()->id)
            ->first()
            ) $canAccess = true;

            if(!$canAccess)
            return response()->json(trans('app.access_denied'), 400);

        }

        if(isset($currentFolder) && $currentFolder != null)
        {
            $tasks = DB::table('tasks')
            ->join('folders', 'tasks.folder_id', '=', 'folders.id')
            ->where('folders.code', '=', $request->folder)
            ->select('tasks.id', 'tasks.title', 'tasks.description', 'tasks.priority', 'tasks.complete', 'folders.colour as label', 'tasks.folder_id')
            ->orderBy('tasks.complete')
            ->orderBy('tasks.priority')
            ->orderBy('tasks.id')
            ->paginate(100);
        }
        else
        {
            $tasks = DB::table('tasks')
            ->join('folders', 'tasks.folder_id', '=', 'folders.id')
            ->where('folders.user_id', Auth::user()->id)
            ->where('tasks.complete', '=', 0)
            ->where('folders.user_id', '=', Auth::user()->id)
            ->select('tasks.id', 'tasks.title', 'tasks.priority', 'tasks.description','tasks.complete', 'folders.colour as label', 'tasks.folder_id')
            ->orderBy('tasks.complete')
            ->orderBy('tasks.priority')
            ->orderBy('tasks.id')
            ->paginate(100);
        }



        return response(compact('tasks'), self::STATUSCODE_SUCCESS);
    }


    /**
     * Create a new task
     * @param  {[type]} Request $request      [description]
     * @return {[type]}         [description]
     */
    public function store(Request $request)
    {
        try
        {
            $result = [];
            $statusCode = self::STATUSCODE_NOTFOUND;

            $rules = [
                'title'	  => 'required',
                'folder'  => 'required'
            ];

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails())
            {
                $result = $validator->errors()->all();
                $statusCode = self::STATUSCODE_BADREQUEST;
            }
            else
            {
                $folder = Folder::find($request->folder);
                if(!$folder)
                return response()->json(trans('app.not_found'), 404);

                $task = new Task([
                    'user_id'   => Auth::user()->id,
                    'folder_id' => $folder->id,
                    'title'     => $request->title
                ]);
                $task->priority = $folder->tasks->count();
                $task->save();
                $task->label    = $folder->colour;



                $statusCode = self::STATUSCODE_SUCCESS;
                $result = $task;

                if($folder->isShared()){
                    $pusher = $this->getPusher();
                    $pusher->trigger('task-channel', 'taskcreated', []);
                }
            }
            return response(compact('task'), $statusCode);
        }
        catch (\Exception $e)
        {
            $result = [ "error" => $e->getMessage()];
            $statusCode = self::STATUSCODE_ERROR;
        }
    }

    /**
    * Mark a task as complete
    * @param  Request $request [description]
    * @param  [type]  $id      [description]
    * @return [type]           [description]
    */
    public function update(Request $request, $id)
    {
        try
        {
            $result = [];
            $statusCode = self::STATUSCODE_NOTFOUND;

            $task = Task::find($id);
            if(!$task){
                $result = trans('app.task_not_found');
            }
            else
            {
                $task->complete = $request->complete;
                $task->update();
                $result = $task;
                $statusCode = self::STATUSCODE_SUCCESS;

                if($task->folder->isShared()){
                    $pusher = $this->getPusher();
                    $pusher->trigger('task-channel', 'taskupdated', []);
                }
            }
        }
        catch (\Exception $e)
        {
            $result = [ "error" => $e->getMessage()];
            $statusCode = self::STATUSCODE_ERROR;
        }
    }



    /**
     * Reorder all tasks in a folder after drag & drop
     * @param  {[type]} Request $request      [description]
     * @return {[type]}         [description]
     */
    public function reorder(Request $request) {
        $items = $request->data;
        foreach($items as $item){
            $task = Task::find((int) $item['id']);
            if($task){
                $task->priority = (int) $item['priority'];
                $task->update();
            }
        }

        $pusher = $this->getPusher();
        $pusher->trigger('task-channel', 'taskupdated', []);
    }


    /**
     * Live update a task to update the title & description
     * @param  {[type]} Request $request      [description]
     * @return {[type]}         [description]
     */
    public function liveUpdate(Request $request)
    {
        try
        {
            $rules = [
                'title'	  => 'required',
                'id'      => 'required'
            ];

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails())
            {
                $result = $validator->errors()->all();
                $statusCode = self::STATUSCODE_BADREQUEST;
                return response()->json($result, $statusCode);
            }

            $task = Task::find($request->id);
            if(!$task){
                return response()->json(trans('app.not_found'), 404);
            }

            $task->title = $request->title;
            $task->description = $request->description ?: "";
            $task->update();

            if($task->folder->isShared()){
                $pusher = $this->getPusher();
                $pusher->trigger('task-channel', 'taskupdated', []);
            }

            return response()->json(trans('app.task_updated'), 200);
        }
        catch (\Exception $e)
        {
            return response()->json($e->getMessage(), 500);
        }
    }
}
