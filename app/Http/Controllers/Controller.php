<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;


    /**
     * Status codes
     */
    const STATUSCODE_SUCCESS 		= 200;
    const STATUSCODE_BADREQUEST 	= 400;
    const STATUSCODE_UNAUTHORISED 	= 401;
    const STATUSCODE_NOTFOUND 		= 404;
    const STATUSCODE_ERROR 			= 500;
    const STATUSCODE_NOTIMPLEMENTED = 501;

    const APIV1_URL                 = "/api/v1/";

    /**
     * Available locales
     */
    const LOCALE_FR = "fr";
    const LOCALE_EN = "en";
    const LOCALE_CN = "cn";
}
