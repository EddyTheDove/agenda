<?php
namespace App\Http\Controllers\Traits;
use App\Functions;
use App\Models\User;

trait ApiToken
{
    public function makeApiToken()
    {
        $token = Functions::randomPassword(60);
        if(User::where('api_token', $token)->first()){
            $this->makeApiToken();
        }
        return $token;
    }
}
