<?php
namespace App\Http\Controllers\Traits;
use App\Models\Folder;

trait CreateFolder
{
    public function createFolder($name, $owner, $parent = null)
    {
        $code = 101102;
        if($last = Folder::orderBy('id', 'desc')->first()){
            $code = $last->code += rand(1,5);
        }

        Folder::create([
            'name'      => $name,
            'code'      => $code,
            'user_id'   => $owner,
            'parent_id' => $parent,
            'colour'    => '#d1d1d1'
        ]);
    }
}
