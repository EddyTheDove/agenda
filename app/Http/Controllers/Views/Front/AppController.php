<?php

namespace App\Http\Controllers\Views\Front;

use Auth;
use Config;
use Session;
use App\Functions;
use App\Models\Taks;
use App\Models\User;
use App\Models\Event;
use App\Models\Folder;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppController extends Controller
{
    /**
     * Serve the app
     * @return [type] [description]
     */
    public function app(){
        $lines = trans('app');
        $lines[] = trans('messages');
        $lines['api'] = self::APIV1_URL;
        $lines['lorem'] = Auth::user()->api_token;
        $lines['labels'] = $this->getLabels();
        $lines['pusher'] = Config('services.pusher.key');
        $lines = json_encode($lines);

        $user = [];
        $user['screen_name'] = Auth::user()->username();
        $user = json_encode($user);

        return view('front.taskimi', compact('lines', 'user'));
    }



    /**
     * Set the default language
     * @param  [type] $locale [description]
     * @return [type]         [description]
     */
    public function translate($locale){
        if(!$locale){
            return redirect()->back();
        }else{
            if (array_key_exists($locale, Config::get('languages'))) {
                Session::set('locale', $locale);
            }
            return redirect()->back();
        }
    }


    public function index(){
        if(Auth::check()){
            return redirect()->to('/app');
        }
        return view('front.pages.home');
    }


    public function login(){
        return view('front.pages.login');
    }

    public function register(){
        return view('front.pages.register');
    }

    public function passwordEmail(){
        return view('front.pages.forgot');
    }

    public function passwordReset($token){
        $user = User::where('api_token', $token)->first();
        if(!$user){
            return redirect()->to('/password/email')->withErrors(['user' => trans('passwords.user')]);
        }
        return view('front.pages.password', compact('token'));
    }


    public function logout(){
        if(Auth::check()){
            Auth::logout();
        }
        return redirect()->to('/');
    }

    public function getLabels()
    {
        return ['#d1d1d1', '#3e4a59', '#0381bd', '#ff6b6b', '#ff6c9d', '#a48ad4', '#44b4a6'];
    }
}
