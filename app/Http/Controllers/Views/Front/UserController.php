<?php

namespace App\Http\Controllers\Views\Front;

use Auth;
use Hash;
use Mail;
use App\Functions;
use App\Models\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Traits\ApiToken;
use App\Http\Controllers\Traits\CreateFolder;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class UserController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins, CreateFolder, ApiToken;

    /**
     * Generate from last user's code
     * @return [type] [description]
     */
    private function makeCode()
    {
        $pin = 110812;
        if($lastCode = User::orderBy('id', 'desc')->first()){
            $pin = $lastCode->code += rand(3, 7);
        }
        return $pin;
    }


    /**
    * On user sign up
    * @param  Request $request [description]
    * @return [type]           [description]
    */
    public function signup(Request $request)
    {
        $rules = [
            'password' => 'required|min:6',
            'email'    => 'required|email|unique:users'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->only('email'));
        }


        $code   = $this->makeCode();
        $token  = $this->makeApiToken();

        $user = User::create([
            'code'		=> $code,
            'api_token' => $token,
            'email'	    => $request->email,
            'password'	=> Hash::make($request->password)
        ]);

        $this->createFolder("General", $user->id);

        Mail::send('emails.activate', ['user' => $user], function($message)
        use ($user)
        {
            $message->to($user->email, $user->email)
            ->subject(trans('emails.welcome_to_taskimy'))
            ->from('no-reply@taskimy.com', 'Taskimi');
        });

        return redirect()->back()->with('message', trans('emails.activation_email_sent'));
    }


    /**
    * Activate new account
    * @param  Request $request [description]
    * @return [type]           [description]
    */
    public function activate($token)
    {
        $user = User::where('api_token', $token)->first();

        if(!$user){
            return redirect()->back()->withErrors(["token" => trans('passwords.token')]);
        }

        $token = $this->makeApiToken();

        $user->status = 'active';
        $user->api_token = $token;
        $user->update();

        Auth::login($user);
        return redirect()->to('/app');

    }


    /**
     * [login description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function login(Request $request)
    {
        $rules = [
            'email'     => 'required|email',
            'password'  => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }


        $email     = $request->email;
        $password  = $request->password;


        $user = User::where('email', $email)->first();
        $hasError = false;

        if(!$user){
            $hasError = true;
            $errorMessage = trans('passwords.user');
        }elseif($user->isBlocked()){
            $hasError = true;
            $errorMessage = trans('passwords.account_blocked');
        }else{
            if(!$user->isActive()){
                $hasError = true;
                $errorMessage = trans('passwords.activate_account');
            }
        }

        if($hasError){
            return redirect()->back()->withInput($request->only('email'))->withErrors(["User" => $errorMessage]);
        }


        if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 'active'], true)) {
            return redirect()->intended('/app');
        }

        return redirect()
        ->back()
        ->withInput($request->only('email'))
        ->withErrors([
            'email' => trans('auth.failed')
        ]);
    }

    /**
     * Send email for forgotten password
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function passwordEmail(Request $request)
    {
        $rules = [
            'email' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }


        $user = User::where('email', $request->email)->first();
        if(!$user){
            return redirect()->back()->withErrors(['User' => trans('passwords.user')]);
        }

        Mail::send('emails.password', ['user' => $user], function($message)
        use ($user)
        {
            $message->to($user->email, $user->email)
            ->subject(trans('emails.password_email_title'))
            ->from('no-reply@taskimy.com', 'Taskimy');
        });

        return redirect()->back()->with('message', trans('emails.password_email_sent'));
    }


    /**
     * [passwordReset description]
     * @param Request $request [description]
     */
    public function passwordReset(Request $request)
    {
        $rules = [
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password',
            'token'    => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }

        $user = User::where('api_token', $request->token)->first();
        if(!$user){
            return redirect()->back()->withErrors(['User' => trans('passwords.user')]);
        }

        $user->password  = Hash::make($request->password);
        $user->api_token = $this->makeApiToken();
        $user->update();

        Auth::login($user);
        return redirect()->to('/app');
    }


    /**
     * Display a user profile
     * @return [type] [description]
     */
    public function show()
    {
        $user = Auth::user();
        return view('front.user.show', compact('user'));
    }

}
