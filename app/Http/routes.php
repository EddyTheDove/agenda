<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(['middleware' => 'locale'], function(){
    Route::get('/', 'Views\Front\AppController@index');
    Route::get('lang/{lang}', 'Views\Front\AppController@translate');

    Route::get('login', 'Views\Front\AppController@login');
    Route::get('logout', 'Views\Front\AppController@logout');

    Route::get('register', 'Views\Front\AppController@register');
    Route::post('register', 'Views\Front\UserController@signup');

    Route::get('activate/{token}', 'Views\Front\UserController@activate');

    Route::post('password/email', 'Views\Front\UserController@passwordEmail');
    Route::get('password/email', 'Views\Front\AppController@passwordEmail');
    Route::post('password/{token}', 'Views\Front\UserController@passwordReset');
    Route::get('password/{token}', 'Views\Front\AppController@passwordReset');

    /**
     * Routes limited to 5 attemps per minute
     */
    Route::group(['middleware' => 'throttle:5'], function(){
        Route::post('login', 'Views\Front\UserController@login');
    });

    /**
     * Angular application
     */
    Route::group(['middleware' => 'auth'], function(){
        Route::get('app', 'Views\Front\AppController@app');


        Route::group(['prefix' => 'account'], function(){
            Route::get('/', function(){
                return redirect()->to('/account/profile');
            });

            Route::get('profile', 'Views\Front\UserController@show');
        });

        /**
         * Api routes
         */
        Route::group(['prefix' => 'api/v1', 'middleware' => 'apitoken'], function(){
            Route::get('home', 'Api\v1\HomeController@index');
            Route::post('tasks/reorder', 'Api\v1\TaskController@reorder');
            Route::post('tasks/liveUpdate', 'Api\v1\TaskController@liveUpdate');

            Route::resource('tasks', 'Api\v1\TaskController');
            Route::resource('shares', 'Api\v1\ShareController');
            Route::resource('events', 'Api\v1\EventController');
            Route::resource('folders', 'Api\v1\FolderController');
            Route::resource('comments', 'Api\v1\CommentController');
        });
    });

    Route::group(['prefix' => 'api/v1'], function(){
        //Route::get('events', 'Api\v1\EventController@index');
    });

});
