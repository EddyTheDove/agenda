<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Event extends Model
{
    protected $fillable = [
        'title', 'start_date', 'end_date', 'hour', 'minutes', 'description', 'status', 'code', 'user_id', 'label'
    ];

    protected $dates = ['start_date', 'end_date'];


    public function scopeActive($query){
        return $query->where('status', 1);
    }

    public function scopeFuture($query){
        return $query->where('start_date', '>=', Carbon::today());
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
