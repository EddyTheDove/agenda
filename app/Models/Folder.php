<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    protected $table = "folders";

    protected $fillable = [
        'name', 'code', 'status', 'colour', 'user_id', 'parent_id'
    ];

    public function isShared(){
        return (bool)$this->shares();
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function shares(){
        return $this->hasMany('App\Models\Shared');
    }

    public function tasks(){
        return $this->hasMany('App\Models\Task');
    }

    public function parent(){
        return $this->belongsTo('App\Models\Folder', 'parent_id');
    }

    public function children(){
        return $this->hasMany('App\Models\Folder', 'parent_id', 'id');
    }


}
