<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shared extends Model
{
    protected $table = "shared_folders";

    protected $fillable = [
        'user_id', 'folder_id', 'status'
    ];

    public function folder(){
        return $this->belongsTo('App\Models\Folder');
    }

    public function users(){
        return $this->hasMany('App\Models\User');
    }
}
