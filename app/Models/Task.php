<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'title', 'user_id', 'folder_id', 'priority', 'complete', 'due_date', 'alert', 'description'
    ];

    public function scopeComplete($query){
        return $query->where('complete', 1);
    }

    public function scopeActive($query){
        return $query->where('complete', 0);
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function folder(){
        return $this->belongsTo('App\Models\Folder');
    }

    public function comments(){
        return $this->hasMany('App\Models\Comment');
    }
}
