<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'email', 'password', 'lastname', 'api_token', 'status', 'level', 'code', 'uuid'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Return a safe username (email, firstname, or lastname)
     * @return [type] [description]
     */
    public function username(){
        if($this->firstname != ""){
            return $this->firstname;
        }elseif($this->lastname != ""){
            return $this->lastname;
        }else{
            $arr = explode('@', $this->email);
            return $arr[0];
        }
    }


    /**
     * Get the user status
     * @return boolean [description]
     */
    public function isActive(){
        return $this->status == 'active';
    }

    public function isBlocked(){
        return $this->status == 'blocked';
    }

    public function folders(){
        return $this->hasMany('App\Models\Folder')->orderBy('name');
    }

    public function comments(){
        return $this->hasMany('App\Models\Comment');
    }

    public function sharedFolders(){
        return $this->belongsToMany('App\Models\Folder', 'shared_folders');
    }

    public function tasks(){
        return $this->hasMany('App\Models\Task')->orderBy('id', 'desc');
    }
}
