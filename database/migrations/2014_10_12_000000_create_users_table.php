<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code');
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('api_token');
            $table->string('locale')->default('en');
            $table->integer('level')->default(1);
            $table->enum('status', ['inactive', 'active', 'blocked'])->default('inactive');
            $table->boolean('email_notifications')->default(false);
            $table->boolean('text_notifications')->default(false);
            $table->boolean('is_premium')->default(false);
            $table->date('premium_end_date')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
