<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('code');
            $table->integer('user_id')->unsigned();
            $table->text('description')->nullable();
            $table->date('start_date')->default(\Carbon\Carbon::now());
            $table->date('end_date')->nullable();
            $table->string('hour')->default('00');
            $table->string('minutes')->default('00');
            $table->string('label')->default('#3e4a59');
            $table->boolean('status')->default(true);
            $table->timestamp('updated_at');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
