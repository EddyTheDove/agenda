<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedFolders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shared_folders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('folder_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->enum('status', ['accepted', 'revoked'])->default('accepted');
            $table->timestamps();
        });



        /**
         * Create foreign Keys
         */
        Schema::table('tasks', function(Blueprint $table) {
            $table->foreign('folder_id','fk_task_folder')->references('id')->on('folders')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });

        Schema::table('shared_folders', function(Blueprint $table) {
            $table->foreign('folder_id','fk_shared_folder')->references('id')->on('folders')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /**
         * Drop foreign keys
         */
        Schema::table('tasks', function(Blueprint $table) {
            $table->dropForeign('fk_task_folder');
        });

        Schema::table('shared_folders', function(Blueprint $table) {
            $table->dropForeign('fk_shared_folder');
        });

        Schema::drop('shared_folders');
    }
}
