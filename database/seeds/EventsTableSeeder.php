<?php

use Illuminate\Database\Seeder;
use App\Models\Event;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $code = 101102;

        Event::create([
            'title'     => 'Today\'s event',
            'code'      => $code,
            'user_id'   => 1,
            'description'     => 'Week 6 assignment is due'
        ]);

        $code++;

        Event::create([
            'title'     => 'AMD Week 6',
            'code'      => $code,
            'user_id'   => 1,
            'description'     => 'Week 6 assignment is due',
            'start_date'=> '2016-04-30'
        ]);
    }
}
