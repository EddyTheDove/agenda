<?php

use Illuminate\Database\Seeder;
use App\Models\Folder;

class FoldersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $code = 101102;

        Folder::create([
            'name'      => 'General',
            'user_id'   => 1,
            'code'      => $code
        ]);

        $code += 3;

        Folder::create([
            'name'      => 'Development',
            'user_id'   => 1,
            'code'      => $code,
            'colour'    => '#0381bd'
        ]);


    }
}
