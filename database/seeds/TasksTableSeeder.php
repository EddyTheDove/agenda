<?php

use Illuminate\Database\Seeder;
use App\Models\Task;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tasks = [
            'Build application foundation',
            'Create main classes and migrations',
            'Create a few seeders for dummy data',
            'Create necessary empty controllers',
            'Write unit tests so they can fail first',
            'Now start test driven development'
        ];

        foreach($tasks as $task){
            Task::create([
                'title'     => $task,
                'user_id'   => 1,
                'folder_id' => 1
            ]);
        }
    }
}
