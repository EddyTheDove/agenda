<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Functions;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $code = 101202;
        User::create([
            'code'      => $code,
            'firstname' => 'Joe',
            'lastname'  => 'Doe',
            'email'     => 'joe@email.com',
            'password'  => Hash::make('pass'),
            'api_token' => Functions::randomPassword(60),
            'level'     => 0,
            'status'    => 'active'
        ]);

        $code += 5;

        User::create([
            'code'      => $code,
            'firstname' => 'Amanda',
            'lastname'  => 'Miler',
            'email'     => 'amiler@email.com',
            'password'  => Hash::make('pass'),
            'api_token' => Functions::randomPassword(60),
            'level'     => 1,
            'status'    => 'active'
        ]);

        $code += 5;

        User::create([
            'code'      => $code,
            'firstname' => 'Jane',
            'lastname'  => 'Doe',
            'email'     => 'jane@email.com',
            'password'  => Hash::make('pass'),
            'api_token' => Functions::randomPassword(60),
            'level'     => 1,
            'status'    => 'active'
        ]);
    }
}
