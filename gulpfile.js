var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir.config.sourcemaps = false;

elixir(function(mix) {
    // mix.sass([
    //     '_flaticon.scss'
    // ]);
    // mix.styles([
    //     'vendor/bootstrap.min.css',
    //     'vendor/icomoon-weather.css',
    //     'vendor/font-awesome.css',
    //     'vendor/simple-line-icons.css',
    //     'vendor/jquery-ui-1.10.3.css',
    //     'vendor/jquery.steps.css',
    //     'vendor/select2-bootstrap.css',
    //     'vendor/select2.css',
    //     'vendor/slidebars.css',
    //     'vendor/tagsinput.css',
    //     'vendor/style-responsive.css',
    //     'vendor/layout-theme-one.css',
    //     'style.css',
    // ], 'public/assets/css/all.min.css');


    mix.scripts([
        'core/jquery.appear.min.js',
        'core/jquery.countTo.min.js',
        'core/jquery.placeholder.min.js',
        'core/jquery.scrollLock.min.js',
        'core/jquery.slimscroll.min.js',
        'core/js.cookie.min.js',
        'core/app.js',
        'core/bootstrap-datepicker.min.js',
        '../../../bower_components/pusher-websocket-iso/dist/web/pusher.js',
        '../../../bower_components/jquery-ui/jquery-ui.min.js',
        //'../../../bower_components/jqueryui-touch-punch/jquery.ui.touch-punch.min.js',

        //Angular files
        'angular/moment.min.js',
        'angular/angular.min.js',
        'angular/angular-route.min.js',
        'angular/angular-animate.min.js',
        'angular/ui-bootstrap-tpls-1.1.2.min.js',
        'angular/angular-moment.min.js',

        //bower components
        '../../../bower_components/pusher-angular/lib/pusher-angular.min.js',
        '../../../bower_components/angular-ui-calendar/src/calendar.js',
        '../../../bower_components/fullcalendar/dist/fullcalendar.min.js',
        '../../../bower_components/fullcalendar/dist/gcal.js',
        '../../../bower_components/angular-notify/dist/angular-notify.min.js',
        '../../../bower_components/angular-ui-router/release/angular-ui-router.min.js',
        '../../../node_modules/trix/dist/trix.js',
        '../../../node_modules/angular-trix/dist/angular-trix.min.js',
        '../../../bower_components/angular-ui-sortable/sortable.min.js',
        '../../../bower_components/angular-elastic/elastic.js',

        //Angular controllers
        'angular/taskimi.js',
        'angular/controllers/home.js',
        'angular/controllers/events.js',
        'angular/controllers/tasks.js',
        'angular/controllers/folders.js',
        'angular/controllers/settings.js',
        'angular/services/folders.js',
        'angular/services/tasks.js',
    ], 'public/assets/js/app.min.js');
});
