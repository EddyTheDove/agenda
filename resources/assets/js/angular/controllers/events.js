angular.module("taskimy.controllers")

/**
* Events index
* @param  {[type]} 'EventsController' [description]
* @param  {[type]} ['$scope'          [description]
* @param  {[type]} '$http'            [description]
* @param  {[type]} function           EventsController($scope, $http [description]
* @return {[type]}                    [description]
*/
.controller('EventsController',
['$scope', '$http', function EventsController($scope, $http){
	$scope.events = [];
	$scope.loading = false;

	$scope.getEvents = function(){
		$scope.loading = true;
		$http.get(_taskimy.api + 'events')
		.success(function(response){
			$scope.events = response.events.data;
			console.log(response);
			$scope.loading = false;
		})
		.error(function(response){
			console.log(response);
			$scope.loading = false;
		});
	}

	$scope.getEvents();
}])


/**
* Create a new event
* @param  {[type]} 'EventCreateController' [description]
* @param  {[type]} ['$scope'               [description]
* @param  {[type]} '$http'                 [description]
* @param  {[type]} function                EventCreateController($scope, $http [description]
* @return {[type]}                         [description]
*/
.controller('EventCreateController',
['$scope', '$http', '$window', '$location', 'notify',
function EventCreateController($scope, $http, $window, $location, notify){
	$window.scrollTo(0, 0);
	$scope.events 	  = [];
	$scope.loading    = false;
	$scope.hours      = [];
	$scope.minutes    = [];
	$scope.labels = _taskimy.labels;



	$scope.buildArrays = function(){
		//Generate hours for the select input
		var hourCounter = 0;
		for(var i = 0; i < 24; i++){
			var arr = [];
			if(i < 10){
				arr.value = "0" + i;
				arr.label = "0" + i + "H";
			}else{
				arr.value = i;
				arr.label = i + "H";
			}
			$scope.hours[hourCounter] = arr;
			hourCounter++;
		}

		//Generate minutes for the select input
		var minuteCounter = 0;
		for(var i = 0; i < 60; i+=5){
			var arr = [];
			if(i < 10){
				arr.value = "0" + i;
				arr.label = "0" + i;
			}else{
				arr.value = i;
				arr.label = i;
			}
			$scope.minutes[minuteCounter] = arr;
			minuteCounter++;
		}
	};


	//Hide/display time
	$scope.toggleTime = function(){
		$scope.schedule.showTime = !$scope.schedule.showTime;
	}

	//Hide/display labels
	$scope.toggleLabels = function(){
		$scope.schedule.showLabels = !$scope.schedule.showLabels;
	}

	//Hide/display events
	$scope.toggleEvents = function(){
		$scope.schedule.showEvents = !$scope.schedule.showEvents;
	}

	//Hide/display editor
	$scope.toggleEditor = function(){
		$scope.schedule.showEditor = !$scope.schedule.showEditor;
	}


	//Persist event
	$scope.save = function(){
		$scope.loading = true;
		$http({
			method: 'POST',
			url: _taskimy.api + 'events',
			data: JSON.stringify({
				title: 		 $scope.schedule.title,
				startdate:   $scope.schedule.startdate,
				enddate:     $scope.schedule.enddate,
				hour:    	 $scope.schedule.hour,
				minutes:     $scope.schedule.minutes,
				short:       $scope.schedule.short,
				label:       $scope.schedule.label,
				description: $scope.schedule.description,
			}),
			headers:{'Content-Type': 'application/json'}
		})
		.success(function(data){
			console.log(data);
			$scope.resetSchedule();
			$scope.getEvents();
			$scope.loading = false;
			notify({
				message: _taskimy.event_created,
				duration: 5000,
				classes: 'bg-success'
			});
		})
		.error(function(data, status){
			console.log(data);
			$scope.loading = false;
			notify({
				message: data.result,
				duration: 30000,
				classes: 'bg-danger'
			});
		});
		$window.scrollTo(0, 0);
	};


	//retrieve 10 upcoming events
	$scope.getEvents = function(){
		$http.get(_taskimy.api + 'events?limit=10')
		.success(function(response){
			$scope.events = response.events;
			console.log(response);
		})
		.error(function(response){
			console.log(response);
		});
	};


	//Set new event default variables
	$scope.resetSchedule = function(){
		$scope.schedule   			= [];
		$scope.schedule.label 		= $scope.labels[1];
		$scope.schedule.hour 		= $scope.hours[0].value;
		$scope.schedule.minutes 	= $scope.minutes[0].value;
		$scope.schedule.showTime    = false;
		$scope.schedule.showLabels  = false;
		$scope.schedule.showEvents  = false;
		$scope.schedule.showEditor  = false;
	}


	//bootstrap the app
	$scope.bootstrap = function(){
		$scope.loading = true;
		$scope.buildArrays();
		$scope.getEvents();
		$scope.resetSchedule();
		$scope.loading = false;
	};

	//Redirect to the event page
	$scope.forward = function(code){
		$location.path("events/" + code);
	}

	//Change schedule label
	$scope.setLabel = function(val){
		$scope.schedule.label = val;
	}


	//Start all variables
	$scope.bootstrap();

}
])




.controller('EventController',
['$scope', '$http', '$stateParams', function EventController($scope, $http, $stateParams){
	$scope.event = [];
	$scope.loading = true;

	$scope.getEvent = function(){
		var _events = [];
		$http.get(_taskimy.api + 'events/' + $stateParams.code)
		.success(function(response){
			$scope.event = response.result;
			$scope.loading = false;
			console.log(response);
		});
	};

	$scope.getEvent();
}])



/**
 * Update event
 * @param  {[type]} 'EventEditController' [description]
 * @param  {[type]} ['$scope'             [description]
 * @param  {[type]} '$http'               [description]
 * @param  {[type]} '$routeParams'        [description]
 * @param  {[type]} 'notify'              [description]
 * @param  {[type]} function              EventEditController($scope, $http, $routeParams, notify [description]
 * @return {[type]}                       [description]
 */
.controller('EventEditController',
['$scope', '$http', '$stateParams', 'notify', function EventEditController($scope, $http, $stateParams, notify){
	$scope.event = [];
	$scope.loading    = false;
	$scope.hours      = [];
	$scope.minutes    = [];
	$scope.labels = _taskimy.labels;

	$scope.event.showTime = false;
	$scope.event.showLabels = false;


	$scope.buildArrays = function(){
		//Generate hours for the select input
		var hourCounter = 0;
		for(var i = 0; i < 24; i++){
			var arr = [];
			if(i < 10){
				arr.value = "0" + i;
				arr.label = "0" + i + "H";
			}else{
				arr.value = i;
				arr.label = i + "H";
			}
			$scope.hours[hourCounter] = arr;
			hourCounter++;
		}

		//Generate minutes for the select input
		var minuteCounter = 0;
		for(var i = 0; i < 60; i+=5){
			var arr = [];
			if(i < 10){
				arr.value = "0" + i;
				arr.label = "0" + i;
			}else{
				arr.value = i+"";
				arr.label = i+"";
			}
			$scope.minutes[minuteCounter] = arr;
			minuteCounter++;
		}
	};


	//Hide/display time
	$scope.toggleTime = function(){
		$scope.event.showTime = !$scope.event.showTime;
	}

	//Hide/display labels
	$scope.toggleLabels = function(){
		$scope.event.showLabels = !$scope.event.showLabels;
	}

	//Hide/display events
	$scope.toggleEvents = function(){
		$scope.event.showEvents = !$scope.event.showEvents;
	}


	$scope.getEvent = function(){
		$http.get(_taskimy.api + 'events/' + $stateParams.code)
		.success(function(response){
			$scope.event = response.result;
			console.log(response);
		})
		.error(function(resp){
			console.log(resp);
		});
	};

	//Change schedule label
	$scope.setLabel = function(val){
		$scope.event.label = val;
	}

	//Persist event
	$scope.save = function(){
		$scope.loading = true;
		$http({
			method: 'PATCH',
			url: _taskimy.api + 'events/' + $scope.event.id,
			data: JSON.stringify({
				title: 		 $scope.event.title,
				startdate:   $scope.event.short_start,
				enddate:     $scope.event.enddate,
				hour:    	 $scope.event.hour,
				minutes:     $scope.event.minutes,
				short:       $scope.event.short,
				label:       $scope.event.label,
				description: $scope.event.description,
			}),
			headers:{'Content-Type': 'application/json'}
		})
		.success(function(data){
			console.log(data);
			$scope.loading = false;
			notify({
				message: _taskimy.event_updated,
				duration: 5000,
				classes: 'bg-success'
			});
		})
		.error(function(data, status){
			console.log(data);
			$scope.loading = false;
			notify({
				message: "Error: " + data.result,
				duration: 30000,
				classes: 'bg-danger'
			});
		});
	};

	$scope.getEvent();
	$scope.buildArrays();
}])




/**
* Calendar View
* @param  {[type]} 'CalendarController' [description]
* @param  {[type]} ['$scope'            [description]
* @param  {[type]} '$http'              [description]
* @param  {[type]} function             CalendarController($scope, $http [description]
* @return {[type]}                      [description]
*/
.controller('CalendarController',
['$scope', '$http', function CalendarController($scope, $http){
	$scope.events = [];
	$scope.eventSources = [];

	var $date   = new Date();
	var d       = $date.getDate();
	var m       = $date.getMonth();
	var y       = $date.getFullYear();

	$scope.buildEvents = function(events){
		for(var i = 0; i < events.length; i++){
			$scope.eventSources.push({
				title: events[i].title,
				start: new Date(events[i].start_date),
				url: '#/events/' + events[i].code,
				allDay: true
			});
		}
		console.log(events);
		console.log($scope.eventSources);

		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: $scope.eventSources
		});
	}


	$scope.getEvents = function(){
		var _events = [];
		$http.get(_taskimy.api + 'events?calendar=true')
		.success(function(response){
			$scope.events = response.events;
			$scope.buildEvents(response.events);
		});
	};

	$scope.getEvents();
}]);
