angular.module("taskimy.controllers")

.controller('FoldersController',
['$scope', '$http', 'notify', 'Folder', function FoldersController($scope, $http, notify, Folder){
	$scope.loading = true;
	$scope.todos   = 0;
	$scope.folders = [];
	$scope.shares = [];

	//Get all tasks from this folder
	$scope.getFolders = function(){
		$http.get(_taskimy.api + 'shares')
		.success(function(response){
			$scope.folders   = response.folders.data;
			$scope.shares    = response.shares.data;
			$scope.loading = false;
			console.log(response);
		})
		.error(function(response){
			console.log(response);
			$scope.loading = false;
		});
	}

	$scope.toggleOptions = function(folder){
		angular.forEach($scope.folders, function(fold){
			if(fold.id != folder.id && fold.options){
				fold.options = false;
			}
		});
		folder.options = !folder.options;
	}

	$scope.delete = function(folder){
		Folder.delete(folder)
		.success(function(response){
			notify({
				message: response,
				duration: 3000,
				classes: 'bg-success'
			});
			var index = $scope.folders.indexOf(folder);
	  		$scope.folders.splice(index, 1);
		})
		.error(function(data){
			notify({
				message: data,
				duration: 5000,
				classes: 'bg-danger'
			});
			console.log(data);
		});
	}

	$scope.leave = function(folder){
		Folder.leave(folder)
		.success(function(response){
			notify({
				message: response,
				duration: 3000,
				classes: 'bg-success'
			});
			var index = $scope.shares.indexOf(folder);
	  		$scope.shares.splice(index, 1);
		})
		.error(function(data){
			notify({
				message: data,
				duration: 5000,
				classes: 'bg-danger'
			});
			console.log(data);
		});
	}

	$scope.getFolders();
}])
/**
 * [Folder controller]
 * @param  {[type]} 'FolderController' [description]
 * @param  {[type]} ['$scope'          [description]
 * @param  {[type]} '$http'            [description]
 * @param  {[type]} '$routeParams'     [description]
 * @param  {[type]} function           FolderController($scope, $http, $routeParams [description]
 * @return {[type]}                    [description]
 */
.controller('FolderController',
['$scope', '$http', '$stateParams', 'Task', 'Folder', 'notify', '$pusher', '$rootScope',
function FolderController($scope, $http, $stateParams, Task, Folder, notify, $pusher, $rootScope){
	$scope.tasks   = [];
	$scope.users   = [];
	$scope.share   = [];
	$scope.loading = true;
	$scope.has_more= false;
	$scope.choices = _taskimy.labels;
	$scope.show_share   = false;
	$scope.current  	= [];
	var client = new Pusher(_taskimy.pusher);
	var pusher = $pusher(client);
	var channel = pusher.subscribe('task-channel');


	//Get the specified folder
	$scope.getFolder = function(){
		$http.get(_taskimy.api + 'folders/' + $stateParams.code)
		.success(function(response){
			$scope.current = response.folder;
			$scope.users   = response.users;
			$scope.resetTodo();
			$scope.resetShare();
			console.log(response);
		})
		.error(function(response){
			console.log(response);
		});
	}


	//Get all tasks from this folder
	$scope.getTasks = function(){
		$http.get(_taskimy.api + 'tasks?folder=' + $stateParams.code)
		.success(function(response){
			$scope.tasks   = response.tasks.data;
			$scope.countAll();
			$scope.loading = false;

			if(response.tasks.next_page_url != null){
				$scope.has_more = true;
			}
		})
		.error(function(response){
			console.log(response);
			$scope.loading = false;
		});
	}

	//How many tasks overall
	$scope.countAll = function(){
		$scope.todos   = 0;
		$scope.done    = 0;
		angular.forEach($scope.tasks, function(task){
			if(task.complete == 1){
				$scope.done++;
			}else{
				$scope.todos++;
			}
		});
	}

	//After saving, reset all todo[] attributes
	$scope.resetTodo = function(){
		$scope.todo    		 = [];
		$scope.todo.label    = $scope.current.colour;
		$scope.todo.folder   = $scope.current.id;
		$scope.todo.complete = 0;
	}

	//Check a single task
	$scope.checkThis = function(task){
		task.complete = 1;
		$scope.done++;
		$scope.todos--;
		$scope.update(task);
	}
	$scope.uncheckThis = function(task){
		task.complete = 0;
		$scope.done--;
		$scope.todos++;
		$scope.update(task);
	}

	//Add new task
	$scope.store = function(){
		$scope.tasks.push($scope.todo);
		console.log($scope.todo);
		Task.save($scope.todo)
		.success(function(data){
			$scope.resetTodo();
			$scope.todos++;

			notify({
				message: _taskimy.task_created,
				duration: 1000,
				classes: 'bg-success'
			});
		})
		.error(function(data){
			notify({
				message: "Error: " + data.result,
				duration: 30000,
				classes: 'bg-danger'
			});
			console.log(data);
		});
	}

	//Update existing task
	$scope.update = function(task){
		Task.update(task)
		.success(function(data){
			notify({
				message: _taskimy.task_updated,
				duration: 3000,
				classes: 'bg-success'
			});
		})
		.error(function(data){
			notify({
				message: "Error: " + data.result,
				duration: 30000,
				classes: 'bg-danger'
			});
			console.log(data);
		});
	}



	$scope.toggleShare = function(){
		$scope.show_share = !$scope.show_share;
	}
	$scope.resetShare = function(){
		$scope.share = [];
		$scope.share.folder = $scope.current.id;
	}

	$scope.shareFolder = function(){
		Folder.share($scope.share)
		.success(function(response){
			notify({
				message: response.result,
				duration: 3000,
				classes: 'bg-success'
			});
			$scope.users.push($scope.share);console.log(response);
			$scope.resetShare();
		})
		.error(function(data){
			notify({
				message: data.result,
				duration: 5000,
				classes: 'bg-danger'
			});
			console.log(data);
		});
	}

	$scope.deleteUser = function(user){
		Folder.unshare(user)
		.success(function(response){
			notify({
				message: response.result,
				duration: 3000,
				classes: 'bg-success'
			});
			var index = $scope.users.indexOf(user);
	  		$scope.users.splice(index, 1);
		})
		.error(function(data){
			notify({
				message: data.result,
				duration: 5000,
				classes: 'bg-danger'
			});
			console.log(data);
		});
	}

	$scope.getFolder();
	$scope.getTasks();

	//pusher listners
	channel.bind('taskcreated', function(data){
		$scope.getTasks();
	});

	channel.bind('taskupdated', function(data){
		$scope.getTasks();
	});

	//Sortable
	$scope.sortableOptions = {
		stop: function(e, ui) {
	    var sortHash = [];
	    for(var i = 0; i < $scope.tasks.length; i++) {
	      item = {
			  'id' : $scope.tasks[i].id,
			  'priority': i
		  };
	      sortHash.push(item);
	    }
	    Task.reorder(sortHash);
	  }
    };


	//Edit a task
	$scope.editTask = function(task){
		$rootScope.editTask = task;
		$rootScope.getComments(task.id);
		$rootScope.showSidebar = true;
	}
}]);
