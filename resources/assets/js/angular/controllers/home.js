angular.module("taskimy.controllers")
.controller('HeadController',
['$scope', '$rootScope','$http', 'notify', '$route',
function HeadController($scope, $rootScope, $http, notify, $route){
	$rootScope._lang 		= _taskimy;
	$rootScope._user 		= _user;
	$rootScope.folders 		= [];
	$rootScope.showSidebar 	= false;
	$rootScope.editTask 	= [];
	$rootScope.comment	 	= [];
	$rootScope.comments	 	= [];

	//Get all user's tasks
	$scope.getFolders = function(){
		$scope.loading = true;
		$http.get(_taskimy.api + 'folders')
		.success(function(response){
			$rootScope.folders = response.folders.data;
		})
		.error(function(response){
			console.log(response);
			$scope.loading = false;
		});
	}

	$scope.toggleSidebar = function(){
		$rootScope.showSidebar = !$rootScope.showSidebar;
	}

	$scope.toggleTitle = function(){
		$rootScope.editTask.editing = true;
		$rootScope.editTask.editDescription = false;
		$rootScope.editTask.editTitle = !$rootScope.editTask.editTitle;
	}

	$scope.toggleDecription = function(){
		$rootScope.editTask.editing = true;
		$rootScope.editTask.editTitle = false;
		$rootScope.editTask.editDescription = !$rootScope.editTask.editDescription;
	}


	//Get all task comments... not really. paginate 5 per 5
	$rootScope.getComments = function(id){
		$scope.loadingComment = true;
		$http.get(_taskimy.api + 'comments?task=' + id)
		.success(function(response){
			$rootScope.comments = response.data;
			$scope.loadingComment = false;
		})
	}

	$scope.updateTask = function(){
		$scope.saving = true;
		$http({
			method: 'POST',
			url: _taskimy.api + 'tasks/liveUpdate',
			data: JSON.stringify({
				id:			   $rootScope.editTask.id,
				title: 		   $rootScope.editTask.title,
				description:   $rootScope.editTask.description
			}),
			headers:{'Content-Type': 'application/json'}
		})
		.success(function(data){
			notify({
				message: _taskimy.task_updated,
				duration: 5000,
				classes: 'bg-success'
			});
			$rootScope.editTask.editTitle = false;
			$rootScope.editTask.editDescription = false;
			$scope.saving = false;
		})
	}

	$scope.addComment = function(){
		$scope.savingComment = true;
		$http({
			method: 'POST',
			url: _taskimy.api + 'comments',
			data: JSON.stringify({
				task_id: $rootScope.editTask.id,
				content: $rootScope.comment.content
			}),
			headers:{'Content-Type': 'application/json'}
		})
		.success(function(data){
			$rootScope.comments.push(data);
			$scope.savingComment = false;
			$rootScope.comment = [];
		})
	}

	//Hide the sidebar when changing view/state
	$scope.$on("$stateChangeStart", function(event){
		$rootScope.showSidebar = false;
	})

	//Hide the menu on mobile when user click on an item
	$rootScope.$on("$stateChangeStart", function(event){
		var menuClasses = angular.element( document.querySelector('#page-container'));
		menuClasses.removeClass('sidebar-o-xs');
	})


	$scope.getFolders();
}])




.controller('HomeController',
['$scope', '$http', function HomeController($scope, $http){
	$scope.stats = [];
	$scope.loading = false;

	$scope.getStats = function(){
		$scope.loading = true;
		$http.get(_taskimy.api + 'home')
		.success(function(response){
			$scope.stats = response.result;
			console.log(response);
			$scope.loading = false;
		})
		.error(function(response){
			console.log(response);
			$scope.loading = false;
		});
	}

	$scope.getStats();
}]);
