angular.module("taskimy.controllers")
.controller('SettingController',
['$scope', '$http', function SettingController($scope, $http){
	$scope.user = [];
	$scope.settings = [];
	$scope.loading = false;

    $scope.getUser = function(){
        $http.get(_taskimy.api + 'users')
        .success(function(response){
			$scope.user = response.user;
			console.log(response);
			$scope.loading = false;
		})
		.error(function(response){
			console.log(response);
			$scope.loading = false;
		});
    }
}]);
