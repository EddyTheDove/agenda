angular.module("taskimy.controllers")

/**
 * TasksController
 * @param  {[type]} 'TasksController' [description]
 * @param  {[type]} ['$scope'         [description]
 * @param  {[type]} '$http'           [description]
 * @param  {[type]} function          TasksController($scope, $http [description]
 * @return {[type]}                   [description]
 */
.controller('TasksController',
['$scope', '$http', 'notify', 'Folder', '$rootScope', function TasksController($scope, $http, notify, Folder, $rootScope){
	$scope.loading 		= false;
	$scope.form_folder  = false;
	$scope.show_folders = true;
	$scope.choices 		= _taskimy.labels;

	//onClick listener to change folder colour
	$scope.setFolderColour = function(val){
		$scope.folder.colour = val;
	}

	$scope.resetFolder = function(){
 		$scope.folder = [];
 		$scope.folder.colour = $scope.choices[0];
 	}

	$scope.editFolder = function(folder){
		angular.forEach($rootScope.folders, function(fold){
			if(fold.edit){
				fold.edit = false;
			}
		})
		folder.edit = !folder.edit;

		if($scope.form_folder){
			$scope.form_folder = false;
		}
	}

	$scope.toggleFolder = function(){
		angular.forEach($rootScope.folders, function(fold){
			if(fold.edit){
				fold.edit = false;
			}
		})
		$scope.form_folder = !$scope.form_folder;
	}

	$scope.toggleFolders = function(){
		$scope.show_folders = !$scope.show_folders;
	}

	$scope.setFoldColour = function(folder, colour){
		folder.colour = colour;
	}

	$scope.close = function(folder){
		folder.edit = false;
	}

	//Create a new folder
	$scope.addFolder = function(){
		Folder.save($scope.folder)
		.success(function(data){
			$scope.folders.push(data.folder);
			$scope.resetFolder();
			console.log(data);
			notify({
				message: _taskimy.folder_created,
				duration: 1000,
				classes: 'bg-success'
			});
		})
		.error(function(data){
			notify({
				message: "Error: " + data.result,
				duration: 30000,
				classes: 'bg-danger'
			});
			console.log(data);
		});
	}

	//Update a single folder
	$scope.updateFolder = function(folder){
		Folder.update(folder)
		.success(function(data){
			$scope.resetFolder();
			$scope.close(folder);

			console.log(data);
			notify({
				message: _taskimy.folder_updated,
				duration: 1000,
				classes: 'bg-success'
			});
		})
		.error(function(data){
			notify({
				message: "Error: " + data.result,
				duration: 30000,
				classes: 'bg-danger'
			});
			console.log(data);
		});
	}

	$scope.resetFolder();
}])




.controller('TaskListController',
['$scope', '$http', 'notify', 'Task', '$rootScope', function TaskListController($scope, $http, notify, Task, $rootScope){
	$scope.tasks   = [];
	$scope.loading = false;
	$scope.form_options = false;
	$scope.has_more     = false;
	$scope.todos   = 0;
	$scope.labels  = [];
	$scope.choices = _taskimy.labels;


	//After saving, reset all todo[] attributes
	$scope.resetTodo = function(){
		$scope.todo    		 = [];
		$scope.todo.label    = $rootScope.folders[0].colour;
		$scope.todo.folder   = $rootScope.folders[0].id;
		$scope.todo.complete = 0;
	}


	//Edit a task
	$scope.editTask = function(task){
		$rootScope.editTask = task;
		$rootScope.getComments(task.id);
		$rootScope.showSidebar = true;
	}


	//Set a task folder
	$scope.setLabel = function(folder){
		$scope.todo.label = folder.colour;
		$scope.todo.folder = folder.id;
		console.log($scope.todo);
	}


	//Get all user's tasks
	$scope.getTasks = function(){
		$scope.loading = true;
		$http.get(_taskimy.api + 'tasks')
		.success(function(response){
			$scope.tasks   = response.tasks.data;
			$scope.resetTodo();
			$scope.loading = false;
			$scope.todos = $scope.tasks.length;
			console.log(response);

			if(response.tasks.next_page_url != null){
				$scope.has_more = true;
			}
		})
		.error(function(response){
			console.log(response);
			$scope.loading = false;
			notify({
				message: response,
				duration: 10000,
				classes: 'bg-danger'
			});
		});
	}


	//Check a single task
	$scope.checkThis = function(task){
		task.complete = 1;
		$scope.update(task);
		$scope.todos--;
	}

	$scope.store = function(){
		$scope.tasks.push($scope.todo);
		Task.save($scope.todo)
		.success(function(data){
			$scope.resetTodo();
			$scope.todos++;

			notify({
				message: _taskimy.task_created,
				duration: 1000,
				classes: 'bg-success'
			});
		})
		.error(function(data){
			notify({
				message: "Error: " + data.result,
				duration: 30000,
				classes: 'bg-danger'
			});
			console.log(data);
		});
	}

	$scope.update = function(task){
		Task.update(task)
		.success(function(data){
			notify({
				message: _taskimy.task_updated,
				duration: 3000,
				classes: 'bg-success'
			});
		})
		.error(function(data){
			notify({
				message: "Error: " + data.result,
				duration: 30000,
				classes: 'bg-danger'
			});
			console.log(data);
		});
	}

	$scope.getTasks();
}]);
