angular.module("taskimi.services")
.factory('Folder', ['$http', function($http){
    return{
        get : function(url){
            return $http.get(url);
        },
        save : function(folder){
            return $http({
    			method: 'POST',
    			url: _taskimy.api + 'folders',
    			data: JSON.stringify({
    				name: folder.name,
    				colour: folder.colour
    			}),
    			headers:{'Content-Type': 'application/json'}
    		})
        },
        update : function(folder){
            return $http({
    			method: 'PATCH',
    			url: _taskimy.api + 'folders/' + folder.id,
    			data: JSON.stringify({
    				name: folder.name,
    				colour: folder.colour
    			}),
    			headers:{'Content-Type': 'application/json'}
    		});
        },
        share : function(share){
            return $http({
    			method: 'POST',
    			url: _taskimy.api + 'shares',
    			data: JSON.stringify({
    				email: share.email,
    				folder: share.folder
    			}),
    			headers:{'Content-Type': 'application/json'}
    		})
        },
        unshare : function(user){
            return $http({
    			method: 'PATCH',
    			url: _taskimy.api + 'shares/' + user.email,
    			data: JSON.stringify({
    				folder: user.folder
    			}),
    			headers:{'Content-Type': 'application/json'}
    		});
        },

        delete : function(folder){
            return $http({
    			method: 'DELETE',
    			url: _taskimy.api + 'folders/' + folder.id,
    			headers:{'Content-Type': 'application/json'}
    		})
        },

        leave : function(folder){
            return $http({
    			method: 'DELETE',
    			url: _taskimy.api + 'shares/' + folder.id,
    			headers:{'Content-Type': 'application/json'}
    		})
        }
    }
}]);
