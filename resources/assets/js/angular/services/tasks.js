angular.module("taskimi.services")
.factory('Task', ['$http', function($http){
    return{
        get : function(url){
            return $http.get(url);
        },
        save : function(task){
            return $http({
    			method: 'POST',
    			url: _taskimy.api + 'tasks',
    			data: JSON.stringify({
    				title: task.title,
    				folder: task.folder
    			}),
    			headers:{'Content-Type': 'application/json'}
    		});
        },
        update : function(task){
            return $http({
    			method: 'PATCH',
    			url: _taskimy.api + 'tasks/' + task.id,
    			data: JSON.stringify({
    				complete: task.complete
    			}),
    			headers:{'Content-Type': 'application/json'}
    		});
        },

        reorder : function(arr){
            return $http({
                method: 'POST',
    			url: _taskimy.api + 'tasks/reorder',
    			data: JSON.stringify({
    				data: arr
    			}),
    			headers:{'Content-Type': 'application/json'}
            })
        }
    }
}]);
