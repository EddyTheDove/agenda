var taskimy = angular.module('taskimy',
['ui.router', 'ngRoute', 'ngAnimate', 'angularMoment', 'ui.bootstrap', 'ui.calendar', 'cgNotify', 'angularTrix', 'pusher-angular', 'ui.sortable', 'monospaced.elastic', 'taskimy.controllers', 'taskimi.services']);

taskimy.config(['$routeProvider', '$httpProvider', '$stateProvider', '$urlRouterProvider', function($routeProvider, $httpProvider, $stateProvider, $urlRouterProvider){
	$urlRouterProvider.when('/tasks', '/tasks/');
	$urlRouterProvider.otherwise("/");
	$stateProvider
	    .state('home', {url: "/", templateUrl: "/assets/angular/view/home.html"})
	    .state('folders', {url: "/folders", templateUrl: "/assets/angular/view/folders/index.html"})
	    .state('settings', {url: "/settings", templateUrl: "/assets/angular/view/users/settings.html"})

	    .state('events', {url: "/events", templateUrl: "/assets/angular/view/events/index.html"})
	    .state('events-add', {url: "/events/new", templateUrl: "/assets/angular/view/events/create.html"})
	    .state('events-calendar', {url: "/events/calendar", templateUrl: "/assets/angular/view/events/calendar.html"})
	    .state('events-show', {url: "/events/:code", templateUrl: "/assets/angular/view/events/show.html"})
	    .state('events-edit', {url: "/events/:code/edit", templateUrl: "/assets/angular/view/events/edit.html"})

	    .state('tasks', {
	      url: "/tasks",
	      templateUrl: "/assets/angular/view/tasks/index.html"
	    })
	    .state('tasks.list', {
	      url: "/",
	      templateUrl: "/assets/angular/view/tasks/list.html",
		  controller: 'TaskListController'
	    })

		.state('tasks.folder', {
	      url: "/:code",
	      templateUrl: "/assets/angular/view/tasks/folder.html"
	    });

}])
.run(['$http', function($http){
	$http.defaults.headers.common['auth-token'] = _taskimy.lorem;
}]);
angular.module("taskimy.controllers", []);
angular.module("taskimi.services", []);


/**
* Filters
* @param  {[type]} 'rawHtml'     [description]
* @param  {[type]} ['$sce'       [description]
* @param  {[type]} function($sce [description]
* @return {[type]}               [description]
*/
taskimy.filter('rawHtml', ['$sce', function($sce){
	return function(val) {
		return $sce.trustAsHtml(val);
	};
}]);


taskimy.directive('folderList', function(){
	return {
		restrict: 'E',
		scope: false,
		templateUrl: '/assets/angular/view/partials/sidefolders.html'
	}
});
