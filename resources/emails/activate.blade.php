<p>{{ trans('emails.activation_email_body') }}</p>
<p><a href="{{ url('activate/'.$user->api_token) }}">{{ url('activate/'.$user->api_token) }}</a></p>
