<?php
return [
    //Dropdown Menu
    "languages"     => "語言",
    "account"       => "Account",
    "settings"      => "Settings",
    "logout"        => "Sign Out",

    //Left Menu
    "add_task"      => "添加新任务",
    "tasks"         => "Tasks",
    "events"        => "Events",
    "folders"       => "Folders",
    "add_folder"    => "New folder",
    "view_all"      => "View all",

    //Taskimy
    "taskimy_welcome"    => "Hi again, welcome to the dashboard.",
    "taskimy_first_task" => "Let's create your first task.",
    "taskimy_profile_description" => "In thins page, you can update your personal details and change your default settings",

    //Buttons
    "btn_save"      => "Save",
    "btn_clear"     => "Clear",
    "btn_cancel"    => "Cancel",

    //Tasks
    "task_empty_folder"     => "No task in this folder",
    "task_empty_remaining"  => "Remaining tasks",
    "task_empty_complete"   => "All clear!",
    "task_search"           => "Search tasks",
    "task_new_task"         => "New task",
    "task_new_event"        => "New event",

    //Forms
    "task_placeholder"      => "What needs to be done?",
    "form_options"          => "Options",
    "form_description"      => "Description",
    "form_type_here"        => "Type here",
    "form_due_date"         => "Choose a due date",
    "form_priority"         => "Priority",
    "form_priority_low"     => "Low",
    "form_priority_medium"  => "Medium",
    "form_priority_high"    => "High",
    "form_set_reminder"     => "Set a reminder for this task",
];
