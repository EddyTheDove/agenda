<?php
return [
    "hi"                => "你好",
    "welcome"           => "欢迎",
    "add_new_task"      => "添加新任务",
    "login"             => "签到",
    "log_out"           => "登出",
    "sign_in"           => "签到",
    "sign_out"          => "登出",
    "fill_details"      => "请输入您的信息",
    "enter_email"       => "电子邮件",
    "enter_password"    => "密码",
    "confirm_password"  => "重复密码",
    "enter_firstname"   => "名字",
    "enter_lastname"    => "姓",
    "forgot_password"   => "F忘记的密码",
    "register"          => "寄存器",
    "no_account"        => "没有帐号？",
    "has_account"       => "已经有一个帐户？",

    //Home page
    "home"              => "家",
    "home_welcome"      => "欢迎！我的名字是Taskimy",
    "home_who-am_i"     => "个人助理",
    "home_what_for"     => "我会帮助你组织你的东西",
    "home_signup"       => "立即注册",
    "home_get_started"  => "我们走吧",
    "home_free"         => "这是免费的",
    "home_intuitive_dashboard" => "易仪表板管理任务和事件",
    "home_taskimy_description" => "Taskimy helps you get organised, from personal to work stuff. It takes the Load off your mind to remember all the due dates and things you wanted to do, so you can actually focus on doing those things. So what are the things you Taskimy can help you do?",
    "home_description_end"     => "With <strong>Taskimy</strong>, never miss a thing again.",
    "home_list_1"       => "Manage your todos lists",
    "home_list_2"       => "Set tasks priorities",
    "home_list_3"       => "Receive reminders for tasks you forget",
    "home_list_4"       => "Schedule events on a calendar (assignments, meetings, movies, dates, bills...)",
    "home_list_5"       => "Sync your events with your mobile devices",
    "home_list_6"       => "Share tasks folders with mates and work in sync",

    //Password Reset
    "pwd_page_title"    => "Password Reset",
    "pwd_btn_send"      => "Send email",
    "pwd_btn_reset"     => "Reset Password",

    //Account
    "profile_current_password" => "Enter current password",
    "profile_new_password"     => "Enter new password"

];
