<?php
return [
    "welcome_to_taskimy"    => "Welcome to Taskimi",
    "activation_email_sent" => "An activation link has been sent to your email",
    "activation_email_body" => "Hi, <br>Thank you for creating an account on Taskimi. Taskily will assist you in organising your stuff so you never miss a thing again. But first, acrtivate your account. <br>Click on the link below",
    "password_email_sent"   => "A link to reset your password has been sent to your email",
    "password_email_body"   => "Hi, you requested a new password on Taskimi. Click on the link below to reset your password",
    "shared_folder"         => "We sent them a notification to share the folder with you",
    "password_email_title"  => "Password reset link",
];
