<?php
return [
    "hi"                => "Hi",
    "welcome"           => "Welcome",
    "add_new_task"      => "Add new task",
    "login"             => "Log in",
    "log_out"           => "Log out",
    "sign_in"           => "Sign In",
    "sign_out"          => "Sign out",
    "fill_details"      => "Please enter your details",
    "enter_email"       => "Email",
    "enter_password"    => "Password",
    "confirm_password"  => "Confirm password",
    "enter_firstname"   => "First name",
    "enter_lastname"    => "Last name",
    "forgot_password"   => "Forgot password",
    "register"          => "Sign Up",
    "no_account"        => "No account?",
    "has_account"       => "Already have an account?",

    //Home page
    "home"              => "Home",
    "home_welcome"      => "Welcome! My name is Taskimi",
    "home_who-am_i"     => "Your personal assistant",
    "home_what_for"     => "I will help you get organised, so you never miss a thing again.",
    "home_signup"       => "Sign Up Now",
    "home_get_started"  => "Let's get started",
    "home_free"         => "It's free",
    "home_intuitive_dashboard" => "Intuitive dashboard to manage your tasks & events",
    "home_taskimy_description" => "Taskimi helps you get organised, from personal to work stuff. It takes the Load off your mind to remember all the due dates and things you wanted to do, so you can actually focus on doing those things. So what can Taskimi help you do?",
    "home_description_end"     => "With <strong>Taskimy</strong>, never miss a thing again.",
    "home_list_1"       => "Manage your todos lists",
    "home_list_2"       => "Set tasks priorities",
    "home_list_3"       => "Receive reminders for tasks you forget",
    "home_list_4"       => "Schedule events on a calendar (assignments, meetings, movies, dates, bills...)",
    "home_list_5"       => "Sync your events with your mobile devices",
    "home_list_6"       => "Share tasks folders with mates and work in sync",

    //Password Reset
    "pwd_page_title"    => "Password Reset",
    "pwd_btn_send"      => "Send email",
    "pwd_btn_reset"     => "Reset Password",

    //Account
    "profile_current_password" => "Enter current password",
    "profile_new_password"     => "Enter new password"

];
