<?php
return [
    "languages" => "Langues",
    "account"   => "Compte",
    "settings"  => "Paramètres",
    "logout"    => "Déconnexion",
    "app_slogan"=> "Votre assistant particulier",
    "confirm"   => "Confirmer",
    "access_denied" => "Accès refusé",
    "title"         => "Titre",
    'description'   => "Description",
    "comments"      => "Commentaires",
    "me"            => "Moi",

    //Left Menu
    "add_task"      => "Nouvelle Tâche",
    "tasks"         => "Tâches",
    "events"        => "Events",
    "folders"       => "Dossiers",
    "add_folder"    => "Nouveau dossier",
    "view_all"      => "Afficher tout",
    "task"          => "Tâche",
    "event"         => "Event",
    "new_event"     => "Nouvel évent",
    "folder"        => "Dossier",
    "hide"          => "Masquer",
    "show"          => "Afficher",
    "edit"          => "Editer",
    "update"        => "Mettre à jour",
    "delete"        => "Supprimer",
    "save"          => "Sauvegarder",
    "cancel"        => "Annuler",
    "todo"          => "A faire",
    "close"         => "Fermer",
    "complete"      => "Fait",
    "all_done"      => "Tout selectionner",
    "unselect_all"  => "Tout déselectionner",
    "not_found"     => "Ressource non trouvée",
    "sync"          => "Sync",

    //Taskimy
    "taskimy_welcome"    => "Salut, moi c'est Taskimy ; ton assistante.",
    "taskimy_first_task" => "Tu n'as pas encore de tâche ? Commençons par en créer une",
    "taskimy_profile_description" => "Cette page te permet de modifier tes infos personnelles, ainsi que tes paramètres par defaut",

    //Buttons
    "btn_save"      => "Enregistrer",
    "btn_clear"     => "Effacer",
    "btn_cancel"    => "Annuler",
    "btn_back"      => "Retour",

    //Tasks
    "task_empty_folder"     => "Ce dossier est vide",
    "task_empty_remaining"  => "Tâches restantes",
    "task_empty_complete"  => "Beau boulot",
    "task_search"           => "Rechercher des tâches",
    "task_new_task"         => "Nouvelle tâche",
    "task_new_event"        => "Nouvel évent",
    "task_created"         => "Tache créée",
    "task_saved"           => "Tache sauvegardée",
    "task_updated"         => "Tache mise à jour",
    "task_deleted"         => "Tache supprimée",
    "task_not_found"       => "Tache non trouvée",
    "enter_description"    => "Cliquez ici pour entrer une description",

    //Forms
    "task_placeholder"    => "Que comptes-tu faire ?",
    "form_options"        => "Paramètres",
    "form_description"    => "Description",
    "form_type_here"      => "Saisissez ici",
    "form_due_date"       => "Date butoire",
    "form_priority"       => "Priorité",
    "form_priority_low"     => "A faire",
    "form_priority_medium"  => "Important",
    "form_priority_high"    => "Urgent",
    "form_set_reminder"     => "Recevoir une alerte pour cette tâche",
    "form_event_title"      => "Titre de l'évent",
    "form_event_short"      => "Resumé de l'évent",
    "form_event_description"=> "Description de l'évent",
    "form_event_start"      => "Date de début",
    "form_event_end"        => "Date de fin",
    "form_event_start_hour" => "Heure de début",
    "form_event_end_hour"   => "Heure de fin",
    "form_event_status"     => "Statut",
    "form_event_calendar"   => "Calendrier",
    "form_optional"         => "Optionel",
    "form_required"         => "Requis",
    "form_show_time"        => "Modifier l'heure",
    "form_hide_time"        => "Masquer l'heure",
    "form_show_labels"      => "Afficher les labels",
    "form_hide_labels"      => "Masquers les labels",
    "form_hide_editor"      => "Masquer l'éditeur",
    "form_show_editor"      => "Afficher l'éditeur",
    "enddate_not_lower"     => "La date de fin ne peut être antérieure à la date de debut",

    //Events
    "calendar"              => "Calendrier",
    "calendar_view"         => "Calendrier",
    "events_list_view"      => "Liste",
    "calendar_day"          => "Jour",
    "calendar_days"         => "Jours",
    "upcoming_events"       => "Events à venir",
    "passed_events"         => "Events passés",
    "passed_recent"         => "Récemment ajoutés",
    "hour"                  => "Heure",
    "hours"                 => "Heures",
    "minute"                => "Minute",
    "minutes"               => "Minutes",
    "today"                 => "Aujourd'hui",
    "tomorrow"              => "Demain",
    "event_created"         => "Event crée",
    "event_saved"           => "Event sauvegardé",
    "event_updated"         => "Event mis à jour",
    "event_deleted"         => "Event supprimé",
    "event_not_found"       => "Event non trouvé",
    "event_details"         => "Détails de l'évent",

    //Colours
    "color_dark"            => "Noir",
    "color_primary"         => "Bleu",
    "color_grey_dark"       => "Sombre",
    "color_purple"          => "Violet",
    "color_pink"            => "Rose",

    //Folders
    "folder_created"        => "Dossier créé",
    "folder_saved"          => "Dossier sauvegardé",
    "folder_moved"          => "Dossier déplacé",
    "folder_deleted"        => "Dossier supprimé",
    "folder_updated"        => "Dossier mis à jour",
    "hide_folders"          => "Masquer les dossiers",
    "show_folders"          => "Afficher les dossiers",
    'enter_email'           => "Entrez l'email",
    "folder_doesnt_exists"  => "Le dossier n'existe pas",
    "folder_user_exist"     => "L'utilisateur partage déjà ce dossier",
    "folder_deleted"        => "Dosser supprimé",
    "my_folders"            => "Mes dossiers",
    "shared_with_me"        => "Dossiers partagés",
    "leave_folder"          => "Ne plus partager",

    //Users
    "share"                 => "Partager",
    "remove_user"           => "Supprimer l'utilisateur",
    "shared_with"           => "Partagé avec",
    "shared"                => "Partagé",
    "share_folder"          => "Partager le dossier",
    "user_doesnt_exist"     => "Cet utilisateur n'exsite pas",
    "user_notified"         => "L'utilisateur a été notifié",
    "user_added"            => "L'utilisateur a été ajouté",
    "user_deleted"          => "L'utilisateur a été supprimé",
    "unauthorised"          => "Action non autorisée",
];
