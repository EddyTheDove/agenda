<?php
return [
    "welcome_to_taskimy"    => "Bienvenue chez Taskimi",
    "activation_email_sent" => "Un lien d'activation vous a été envoyé",
    "activation_email_body" => "Bonjour, <br>Merci de vous être inscrit(e) sur Taskimi. Taskimi prendra soin de vous et vous aidera à mieux vous organiser pour que vous ne loupiez plus jamais quoi que ce soit. Mais d'abord, veuillez activer votre compte en cliquant sur le lien ci-dessous",
    "password_email_title"  => "Réinitialisation du mot de passe",
    "password_email_sent"   => "Un lien vous a été envoyé afin de réinitialiser votre mot de passe",
    "password_email_body"   => "Bonjour, <br> Vous avez requis un lien de réinitialiastion de mot de passe. Veuillez cliquer sur le lien ci-dessous pour réinitialiser votre mot de passe",
    "shared_folder"         => "L'utilisateur a été mis au courant du dossier partagé",
];
