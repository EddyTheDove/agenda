<?php
return [
    "hi"                => "Bonjour",
    "welcome"           => "Bienvenue",
    "add_new_task"      => "Nouvelle Tache",
    "login"             => "Connectez-vous",
    "log_out"           => "Déconnexion",
    "sign_in"           => "Connexion",
    "sign_out"          => "Déconnexion",
    "fill_details"      => "Veuillez saisir vos détails",
    "enter_email"       => "Email",
    "enter_password"    => "Mot de passe",
    "confirm_password"  => "Confirmez le mot de passe",
    "enter_firstname"   => "Prénom",
    "enter_lastname"    => "Nom",
    "forgot_password"   => "Mot de passe oublié",
    "register"          => "Inscrivez-vous",
    "no_account"        => "Pas de compte ?",
    "has_account"       => "Déja inscrit ?",

    //Home page
    "home"              => "Accueil",
    "home_welcome"      => "Bienvenue, moi c'est Taskimi",
    "home_who-am_i"     => "Ton assistante",
    "home_what_for"     => "Je vais t'aider à t'organiser afin que tu sois toujours à jour",
    "home_signup"       => "Inscris-toi Maintenant",
    "home_get_started"  => "C'est parti",
    "home_free"         => "C'est gratuit",
    "home_intuitive_dashboard" => "Un tableau de bord intuitif pour la gestion de vos tâches et events",
    "home_taskimy_description" => "<strong>Taskimi</strong> est un assistant personnel vous permetant de mieux gérer vos tâches et vos events. <strong>Taskimi</strong> enlève de votre tête le stress de l'oubli et vous permet de vous concentrer sur ce que vous avez à faire. En fait <strong>Taskimi</strong> vous permet de : ",
    "home_description_end"     => "Grâce à <strong>Taskimi</strong>, restez à jour",
    "home_list_1"       => "Manager vos listes de todo",
    "home_list_2"       => "Definir vos priorités",
    "home_list_3"       => "Recevoir des rappels pour vos tâches importantes",
    "home_list_4"       => "Planifier vos événements (devoirs, rendez-vous, films...)",
    "home_list_5"       => "Synchroniser vos évènements avec vos appareils mobiles",
    "home_list_6"       => "Partager des dossiers avec des amis et collègues",

    //Password Reset
    "pwd_page_title"    => "Mot de passe oublié",
    "pwd_btn_send"      => "Envoyer l'email",
    "pwd_btn_reset"     => "Réinitialiser",

    //Account
    "profile_current_password" => "Mot de passe actuel",
    "profile_new_password"     => "Nouveau mot de passe",

];
