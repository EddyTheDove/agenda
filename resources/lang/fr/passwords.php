<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Les mots de passe doit contenir au moins 6 caractères et etre identiques.',
    'reset' => 'Le mot de passe a été réinitialiser',
    'sent' => 'Nous avons envoyé un lien de réinitialisation de votre mot de passe',
    'token' => 'Le token spécifié est invalide',
    'user' => "Aucun utilisateur n'a pu être trouvé avec cet email",
    "activate_account" => "Veuillez activer votre compte en cliquant sur le lien reçu par email",
    "account_blocked" => "Désoleé votre compte a été désactivé. Bien vouloir nous contacter pour plus d'information",

];
