<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>Taskimi - Let's get organised</title>

    <meta name="description" content="trans('app.app_slogan')">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="/assets/img/favicons/favicon.png">

    <link rel="icon" type="image/png" href="/assets/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/assets/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/assets/img/favicons/favicon-96x96.png" sizes="96x96">

    <link rel="apple-touch-icon" sizes="57x57" href="/assets//img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets//img/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets//img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets//img/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets//img/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets//img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets//img/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets//img/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets//img/favicons/apple-touch-icon-180x180.png">

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    <link rel="stylesheet" id="css-main" href="/assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" id="css-main" href="/assets/css/oneui.css">
    <link rel="stylesheet" id="css-main" href="/assets/css/style.css">
</head>
<body>
    <div id="page-container" class="sidebar-l sidebar-mini sidebar-o header-navbar-fixed">

        @include('front.includes.aside')
        @include('front.includes.header')

        <!-- Main Container -->
        <main id="main-container">

            <div class="content">
                <div class="">
                    <div class="row">
                        <div class="col-sm-4 col-md-3 hidden-xs">
                            <div class="block">
                                <div class="block-header bg-darker text-white">
                                    <ul class="block-options">
                                        <li>
                                            <a href="" class="text-white"><i class="flaticon-folder"></i> <i class="flaticon-cross"></i></a>
                                        </li>
                                    </ul>
                                    <i class="flaticon-folder-3"></i> {{ trans('app.folders') }}
                                </div>
                                <div class="block-content block-content-full">
                                    <ul class="list-unstyled bold folders">
                                        <li>
                                            <a href="#/folders/201134">
                                                <i class="flaticon-arrows-1"></i> General
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#/folders/201134">
                                                <i class="flaticon-arrows-1"></i> School
                                            </a>
                                        </li>
                                        <li class="pl-20">
                                            <a href="#/folders/201134">
                                                <i class="flaticon-arrows-1"></i> .NET
                                            </a>
                                        </li>
                                        <li class="pl-20">
                                            <a href="#/folders/201134">
                                                <i class="flaticon-arrows-1"></i> C++
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#/folders/201134">
                                                <i class="flaticon-arrows-1"></i> Work
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#/folders/201134">
                                                <i class="flaticon-arrows-1"></i> Movies
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-7 col-md-9">

                            <!--Search form-->
                            <div class="mb-20">
                                <form class="form-horizontal" action="base_pages_search.html" method="post">
                                    <div class="form-material form-material-primary input-group remove-margin-t remove-margin-b">
                                        <input class="form-control" type="text" id="base-material-text" name="base-material-text" placeholder="{{ trans('app.task_search') }}...">
                                        <span class="input-group-addon"><i class="si si-magnifier"></i></span>
                                    </div>
                                </form>
                            </div>
                            <!--/Search form-->


                            <!--All tasks-->
                            <div class="block block-bordered">
                                <div class="block-header bg-darker">
                                    <small class="text-white"><i class="flaticon-folder-3"></i> General / {{ trans('app.task_empty_folder') }}</small>
                                </div>

                                <div class="block-content no-padding">
                                    <div class="">
                                        <table class="js-table-checkable table table-hover table-vcenter table-striped">
                                            <tbody>
                                                <tr>
                                                    <td class="ellipsis">
                                                        <label class="css-input css-checkbox css-checkbox-primary">
                                                            <input type="checkbox"><span></span>
                                                        </label>
                                                        Welcome to our subcriptions
                                                    </td>
                                                </tr>

                                                <tr class="ellipsis">
                                                    <td>
                                                        <label class="css-input css-checkbox css-checkbox-primary">
                                                            <input type="checkbox"><span></span>
                                                        </label>
                                                        We are glad you decided to go with a vip account as we didn't expect students to do something like that amazing
                                                    </td>
                                                </tr>

                                                <tr class="ellipsis">
                                                    <td>
                                                        <label class="css-input css-checkbox css-checkbox-primary">
                                                            <input type="checkbox"><span></span>
                                                        </label>
                                                        An update is under way for your app
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="ellipsis">
                                                        <label class="css-input css-checkbox css-checkbox-primary">
                                                            <input type="checkbox"><span></span>
                                                        </label>
                                                        Due to the fact that you are a great
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <!--new task form-->
                                    <form ng-submit="save()" class="mb-20 pd-10">
                                        <div class="form-group">
                                            <div class="form-material floating">
                                                <input class="form-control" type="text" id="material-email2" name="title">
                                                <label for="material-email2">{{ trans('app.task_placeholder') }} {{ trans('app.form_type_here') }}</label>
                                            </div>
                                        </div>

                                        <div class="">
                                            <a href="#" class="text-purple bold show_form font-s13 uppercase">
                                                <i class="flaticon-tool"></i> {{ trans('app.form_options') }}
                                            </a>
                                        </div>

                                        <div class="form_options mt-20">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-9">
                                                    <div class="form-group">
                                                        <div class="form-material floating">
                                                            <input class="js-datepicker form-control" type="text" id="example-datepicker6" name="example-datepicker6" data-date-format="dd-mm-yyyy">
                                                            <label for="example-datepicker6">{{ trans('app.form_due_date') }}</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 col-md-3">
                                                    <label for="">{{ trans('app.form_priority') }}</label>
                                                    <div class="styled-select mb-10">
                                                        <select name="city" class="form-control">
                                                            <option value="1">1 - {{ trans('app.form_priority_low') }}</option>
                                                            <option value="2">2 - {{ trans('app.form_priority_medium') }}</option>
                                                            <option value="3">3 - {{ trans('app.form_priority_high') }}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="">
                                                <div class="">
                                                    <label class="css-input switch switch-sm switch-purple">
                                                        <input type="checkbox"><span></span> {{ trans('app.form_set_reminder') }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="text-right mb-20">
                                            <button type="submit"  class="btn btn-blue btn-lg">
                                                <i class="flaticon-technology-1"></i> {{ trans('app.btn_save') }}
                                            </button>
                                        </div>
                                    </form>
                                    <div class="clearfix"></div>
                                </div>
                                <!--/content-->
                            </div>
                            <!--/All tasks-->

                            <div class="mt-10">
                                <button type="button" name="button"  class="btn btn-red btn-lg">
                                    <i class="flaticon-interface-1"></i> Select all
                                </button>
                                <button type="button" name="button"  class="btn btn-blue btn-lg">
                                    <i class="flaticon-clock"></i> View completed
                                </button>
                            </div>


                        </div>
                    </div>
                </div>
            </div>


        </main>

        <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-white clearfix text-center">
            <div class="text-dark">
                &copy; 2016 Taskimi. With <i class="flaticon-shapes-1"></i> from Sydney
            </div>
        </footer>
    </div>



    <script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="/assets/js/app.min.js"></script>


    <script src="/assets/angular/taskimi.js"></script>
    <script src="/assets/angular/controllers/tasks.js"></script>
    <script src="/assets/angular/controllers/folders.js"></script>
    <script src="/assets/angular/controllers/events.js"></script>
    <script src="/assets/angular/controllers/home.js"></script>
    <script src="/assets/angular/services/tasks.js"></script>
    <script src="/assets/angular/services/folders.js"></script>




    <script>
    $('.form_options').hide();
    $('.show_form').on('click', function(){
        $('.form_options').slideToggle('fast');
    });
    var _taskimy = <?php echo $lines; ?>

    $(function () {
        App.initHelpers(['datepicker']);
    });
    </script>
</body>
</html>
