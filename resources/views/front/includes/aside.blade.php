<!-- Sidebar -->
<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="side-header side-content">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>

                <a class="h5 text-white" href="/">
                    <img src="/assets/img/taskimy.png" width="35" style="margin-left:-5px;"/> <span class="h4 font-w600 sidebar-mini-hide">Taskimi</span>
                </a>
            </div>
            <!-- END Side Header -->

            <!-- Side Content -->
            <div class="side-content">
                <ul class="nav-main">
                    <li>
                        <a href="#/tasks">
                            <i class="flaticon-interface-1"></i>
                            <span class="sidebar-mini-hide">{{ trans('app.tasks') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="#/events">
                            <i class="flaticon-business "></i>
                            <span class="sidebar-mini-hide">{{ trans('app.events') }}</span>
                        </a>
                    </li>

                    <li>
                        <a href="#/events">
                            <i class="flaticon-folder-1 "></i>
                            <span class="sidebar-mini-hide">{{ trans('app.folders') }}</span>
                        </a>
                    </li>

                    <li class="nav-main-heading"><span class="sidebar-mini-hide">{{ trans('app.settings') }}</span></li>

                    <li>
                        <a href="/account">
                            <i class="si si-user"></i>
                            <span class="sidebar-mini-hide">{{ Auth::user()->username() }}</span>
                        </a>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                            <i class="si si-globe"></i>
                            <span class="sidebar-mini-hide">{{ trans('app.languages') }}</span>
                        </a>
                        <ul>
                            <li>
                                <a href="/lang/en">
                                    <i class="si si-globe"></i> English
                                </a>
                            </li>
                            <li>
                                <a href="/lang/fr">
                                    <i class="si si-globe"></i> Français
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="/logout"><i class="si si-power"></i><span class="sidebar-mini-hide">{{ trans('app.logout') }}</span></a>
                    </li>
                </ul>
            </div>
            <!-- END Side Content -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>
<!-- END Sidebar -->
