<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-white clearfix text-center">
    <div class="text-dark">
        &copy; 2016 Taskimi. With <i class="flaticon-shapes-1"></i> from Sydney
    </div>
</footer>
