<header id="header-navbar" class="content-mini-full">
    <div class="">
        <ul class="nav-header pull-right">
            <li>
                <a href="/login" class="active active-blue text-white">
                    <i class="flaticon-arrows-2"></i> {{ trans('messages.sign_in') }}
                </a>
            </li>
            <li class="dropdown">
                <a type="button" data-toggle="dropdown" href="javascript;">{{ trans('app.languages') }} <span class="flaticon-arrows-4"></span></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a tabindex="-1" href="/lang/en">
                            <i class="si si-globe"></i> English
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a tabindex="-1" href="/lang/fr">
                            <i class="si si-globe"></i> Français
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a tabindex="-1" href="/lang/cn">
                            <i class="si si-globe"></i> 繁體中文
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="nav-header pull-left hidden-xs bg-blue">
            <li>
                <a class="h5 text-white" href="/">
                    <img src="/assets/img/taskimy.png" width="45" alt="" /> <span class="h4 font-w600 sidebar-mini-hide">Taskimi</span>
                </a>
            </li>
        </ul>
    </div>
</header>
