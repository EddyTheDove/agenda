<header id="header-navbar" class="">

    <ul class="nav-header pull-right">
        <li class="bold">
            <a href="#">
                <i class="flaticon-music-1"></i>
            </a>
        </li>
        <li class="dropdown active active-blue">
            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" type="button">
                <i class="flaticon-cross"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    <a tabindex="-1" href="/app/#/tasks">
                        <i class="flaticon-interface-1"></i> {{ trans('app.task_new_task') }}
                    </a>
                </li>
                <li>
                    <a tabindex="-1" href="/app/#/events">
                        <i class="flaticon-business"></i> {{ trans('app.task_new_event') }}
                    </a>
                </li>
            </ul>
        </li>
        <li class="hidden-md hidden-lg active active-dark">
            <a data-toggle="layout" data-action="sidebar_toggle" type="button" href="javascript:;" class="hidden-md hidden-lg">
                <i class="flaticon-bars"></i>
            </a>
        </li>
    </ul>
</header>
