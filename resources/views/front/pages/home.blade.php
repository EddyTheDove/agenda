@extends('front.template')
@section('head')
<title>Taskimi - {{ trans('messages.home') }}</title>
@stop

@section('body')

<div class="bg-image bg-image-parallax" style="background-image: url('assets/img/bg2.jpg');">
    <div class="bg-primary-dark-op">
        <section class="content content-full content-boxed overflow-hidden">
            <!-- Section Content -->
            <div class="push-100-t push-50 text-center">
                <h1 class="h2 text-white push-10 visibility-hidden" data-toggle="appear" data-class="animated fadeInDown">{{ trans('messages.home_welcome') }}</h1>
                <h4 class="h4 text-white push-10 visibility-hidden" data-toggle="appear" data-class="animated fadeInLeft">{{ trans('messages.home_who-am_i') }}</h4>

                <div class="">
                    <img src="/assets/img/taskimy-default.png" alt="" />
                </div>

                <h2 class="h5 text-white-op push-50 visibility-hidden" data-toggle="appear" data-class="animated fadeInDown">{{ trans('messages.home_what_for') }}</h2>
                <a class="btn btn-rounded w-200 btn-lg btn-white visibility-hidden text-purple bold" data-toggle="appear" data-class="animated bounceIn" data-timeout="800" href="/register">
                    {{ trans('messages.home_signup') }}
                </a>
                <h4 class="h5 text-white mt-10">{{ trans('messages.home_free') }}</h4>
            </div>

        </section>
    </div>
</div>

<!-- Get Started -->
<div class="bg-grey">
    <section class="content content-full content-boxed">
        <!-- Section Content -->
        <div class="push-20-t push-20 text-center">
            <h3 class="h4 push-20 visibility-hidden" data-toggle="appear">
                {{ trans('messages.home_intuitive_dashboard') }}
            </h3>


            <div class="row">
                <div class="col-sm-6">
                    <div class="">
                        <img src="/assets/img/screen1.png"  class="img-responsive"/>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="block block-bordered text-left">
                        <div class="block-content">
                            <p>
                                {!! trans('messages.home_taskimy_description') !!}
                            </p>

                            <table class="table table-striped">
                                <tr>
                                    <td>
                                        <i class="flaticon-interface-1"></i> {{ trans('messages.home_list_1') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="flaticon-interface-1"></i> {{ trans('messages.home_list_2') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="flaticon-interface-1"></i> {{ trans('messages.home_list_3') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="flaticon-interface-1"></i> {{ trans('messages.home_list_4') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="flaticon-interface-1"></i> {{ trans('messages.home_list_5') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="flaticon-interface-1"></i> {{ trans('messages.home_list_6') }}
                                    </td>
                                </tr>
                            </table>

                            <p>
                                {!! trans('messages.home_description_end') !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-40">
                <a class="btn btn-rounded w-200 btn-lg btn-purple visibility-hidden" data-toggle="appear" data-class="animated fadeIn" data-timeout="300" href="/register">
                    {{ trans('messages.home_signup') }}
                </a>
                <h4 class="h4 text-dark mt-10 mb-20">{{ trans('messages.home_free') }}</h4>
            </div>
        </div>
        <!-- END Section Content -->
    </section>
</div>
<!-- END Get Started -->
@stop
