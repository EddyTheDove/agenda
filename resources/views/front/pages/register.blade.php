<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>Taskimi - {{ trans('messages.register') }}</title>

    <meta name="description" content="{{ trans('app.app_slogan') }}">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

    <link rel="shortcut icon" href="/assets/img/favicons/favicon.png">

    <link rel="icon" type="image/png" href="/assets/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/assets/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/assets/img/favicons/favicon-96x96.png" sizes="96x96">

    <link rel="apple-touch-icon" sizes="57x57" href="/assets//img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets//img/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets//img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets//img/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets//img/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets//img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets//img/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets//img/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets//img/favicons/apple-touch-icon-180x180.png">

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    <link rel="stylesheet" id="css-main" href="/assets/css/flaticon.css">
    <link rel="stylesheet" id="css-main" href="/assets/css/main.css">
    <link rel="stylesheet" id="css-main" href="/assets/css/style.css">
</head>
<body class="bg-image2">
    <div id="page-container" class="sidebar-l header-navbar-fixed">

        @include('front.includes.guestheader')


        <!-- Main Container -->
        <main id="main-container">
            <!-- Login Content -->
        <div class="content overflow-hidden">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-1">
                    <div class="text-center h3">
                        <h1 class="h2 font-w600 text-white">{{ trans('messages.register') }}</h1>
                    </div>

                    <div class="mt-10">
                        @include('errors.list')
                    </div>

                    <div class="block block-bordered animated fadeIn mt-20">
                        <div class="block-content text-center">
                            <div class="row">
                                <div class="col-sm-12 mb-10">
                                    <a href="#" class="btn btn-lg btn-block btn-blue">
                                        <i class="flaticon-social-3"></i> Facebook
                                    </a>
                                </div>
                                <div class="col-sm-12">
                                    <a href="#" class="btn btn-lg btn-block btn-red">
                                        <i class="flaticon-social-4"></i> Google +
                                    </a>
                                </div>
                            </div>


                            <form class="js-validation-login form-horizontal mt-20" action="" method="post">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material form-material-primary floating">
                                            <input class="form-control" type="email" id="login-username" name="email" value="{{ old('email') }}" required>
                                            <label for="login-username">{{ trans('messages.enter_email') }}</label>
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        <div class="form-material form-material-primary floating">
                                            <input class="form-control" type="password" id="login-password" name="password" required>
                                            <label for="login-password">{{ trans('messages.enter_password') }}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="">
                                        <button class="btn btn-purple btn-lg w-200" type="submit">{{ trans('messages.register') }} <i class="si si-login"></i></button>
                                    </div>
                                </div>
                                <div class="text-center mb-20">
                                    <p>{{ trans('messages.has_account') }} <a href="/login">{{ trans('messages.login') }} </a></p>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Login Block -->
                </div>
            </div>
        </div>
        <!-- END Login Content -->

        </main>

        @include('front.includes.footer')

    </div>


    <script src="/assets/js/core/jquery.min.js"></script>
        <script src="/assets/js/core/bootstrap.min.js"></script>
        <script src="/assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="/assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="/assets/js/core/jquery.placeholder.min.js"></script>
    <script src="/assets/js/app.js"></script>
    <script type="text/javascript">
</body>
</html>
