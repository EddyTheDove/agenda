<!DOCTYPE html>
<html class="no-focus" ng-app="taskimy">
<head>
    <meta charset="utf-8">
    <title>Taskimi - <?php echo trans('app.app_slogan'); ?></title>

    <meta name="description" content="<?php echo trans('app.home_intuitive_dashboard'); ?>">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="/assets/img/favicons/favicon.png">

    <link rel="icon" type="image/png" href="/assets/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/assets/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/assets/img/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/assets/img/favicons/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="/assets/img/favicons/favicon-192x192.png" sizes="192x192">

    <link rel="apple-touch-icon" sizes="57x57" href="/assets//img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets//img/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets//img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets//img/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets//img/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets//img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets//img/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets//img/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets//img/favicons/apple-touch-icon-180x180.png">

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    <link rel="stylesheet" id="css-main" href="/assets/css/flaticon.css">
    <link rel="stylesheet" id="css-main" href="/assets/css/fullcalendar.min.css">
    <link rel="stylesheet" href="/assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css">
    <link href="/assets/css/bootstrap-wysihtml5.css" rel="stylesheet">
    <link href="/assets/css/angular-notify.css" rel="stylesheet">
    <link href="/assets/css/trix.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/ng-tags-input.min.css">
    <link rel="stylesheet" id="css-main" href="/assets/css/main.css">
    <link rel="stylesheet" href="/assets/css/style.css">
</head>
<body ng-controller='HeadController'>
    <div class="loader">
        <div class="fading-line"></div>
    </div>

    <div id="page-container" class="sidebar-l sidebar-mini sidebar-o header-navbar-fixed side-overlay-o-xs" ng-class="{'side-overlay-o': showSidebar}">

        <!-- Side Overlay-->
            <aside id="side-overlay">
                <!-- Side Overlay Scroll Container -->
                <div id="side-overlay-scroll">
                    <!-- Side Content -->
                    <div class="side-content remove-padding-t">
                        <!-- Notifications -->
                        <div class="block pull-r-l">
                            <button class="btn btn-sm btn-dark pull-right no-border-radius" ng-click="toggleSidebar()">
                                <i class="fa fa-times"></i> {{ _lang.close }}
                            </button>

                            <div class="block-content clearfix">
                                <form ng-submit="updateTask()">
                                    <h4 class="h5 mb-10 text-primary">{{ _lang.task }}</h4>


                                    <h4 class="h5" ng-if="!editTask.editTitle" ng-click="toggleTitle()">
                                        <span class="dotted" style="color:#333;">
                                            <i class="si si-pencil"></i>
                                            {{ editTask.title }}
                                        </span>
                                    </h4>
                                    <input type="text" name="title" ng-model="editTask.title" required class="form-control" value="{{ editTask.title }}" ng-if="editTask.editTitle">

                                    <h4 class="h5 mb-10 mt-20 text-primary">{{ _lang.form_description }}</h4>

                                    <div class="fs-14 mb-10" ng-if="!editTask.editDescription">
                                        <p  class="dotted" style="color:#333;" ng-click="toggleDecription()">
                                            <i class="si si-pencil"></i> {{ editTask.description ? editTask.description : _lang.enter_description }}
                                        </p>
                                    </div>
                                    <textarea name="content" rows="2" msd-elastic class="form-control no-resize" ng-model="editTask.description" placeholder="{{ _lang.form_type_here }}" ng-if="editTask.editDescription">
                                        {{ editTask.description }}
                                    </textarea>

                                    <div class="text-center">
                                        <img ng-src="/assets/img/22.gif" width="40" ng-if="saving"/>
                                    </div>

                                    <button type="submit" class="btn btn-blue mt-10 pull-right" ng-show="editTask.editing">{{ _lang.btn_save }}</button>
                                </form>
                            </div>
                        </div>

                        <div class="block pull-r-l mt-20">
                            <div class="block-header bg-gray-lighter">
                                <h3 class="block-title text-primary">{{ _lang.comments }}</h3>
                            </div>

                            <div class="block-content block-content-full">
                                <div class="text-center">
                                    <img ng-src="/assets/img/22.gif" width="40" ng-if="loadingComment"/>
                                </div>

                                <ul class="nav-users mb-20" ng-if="comments.length">
                                    <li ng-repeat="com in comments" class="mb-10">
                                        <div class="font-s13 font-w600 ellipsis">{{ com.author }} - {{ com.date }}</div>
                                        <div class="font-s13 font-w400 text-muted">{{ com.content }}</div>
                                    </li>
                                </ul>

                                <form ng-submit="addComment()">
                                    <textarea name="content" rows="2" msd-elastic class="form-control no-resize" ng-model="comment.content" placeholder="{{ _lang.form_type_here }}"></textarea>

                                    <div class="text-center">
                                        <img ng-src="/assets/img/22.gif" width="40" ng-if="savingComment"/>
                                    </div>
                                    <button type="submit" class="btn btn-blue mt-10 pull-right">{{ _lang.btn_save }}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- END Side Content -->
                </div>
                <!-- END Side Overlay Scroll Container -->
            </aside>
            <!-- END Side Overlay -->



        <nav id="sidebar">
            <div id="sidebar-scroll">
                <div class="sidebar-content">
                    <div class="side-header side-content">
                        <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                            <i class="fa fa-times"></i>
                        </button>

                        <a class="h5 text-white" href="#/">
                            <img src="/assets/img/taskimy.png" width="35" style="margin-left:-5px;"/> <span class="h4 font-w600 sidebar-mini-hide">Taskimi</span>
                        </a>
                    </div>
                    <!-- END Side Header -->

                    <!-- Side Content -->
                    <div class="side-content">
                        <ul class="nav-main">
                            <li>
                                <a href="#/tasks">
                                    <i class="flaticon-interface-1"></i>
                                    <span class="sidebar-mini-hide">{{ _lang.tasks }}</span>
                                </a>
                            </li>
                            <li>
                                <a href="#/events">
                                    <i class="flaticon-business "></i>
                                    <span class="sidebar-mini-hide">{{ _lang.events }}</span>
                                </a>
                            </li>

                            <li>
                                <a href="#/folders">
                                    <i class="flaticon-folder-1 "></i>
                                    <span class="sidebar-mini-hide">{{ _lang.folders }}</span>
                                </a>
                            </li>

                            <li class="nav-main-heading"><span class="sidebar-mini-hide">{{ _lang.settings }}</span></li>

                            <li>
                                <a href="#/settings">
                                    <i class="si si-user"></i>
                                    <span class="sidebar-mini-hide">{{ _user.screen_name }}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                                    <i class="si si-globe"></i>
                                    <span class="sidebar-mini-hide">{{ _lang.languages }}</span>
                                </a>
                                <ul>
                                    <li>
                                        <a href="/lang/en">
                                            <i class="si si-globe"></i> English
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/lang/fr">
                                            <i class="si si-globe"></i> Français
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="/logout"><i class="si si-power"></i><span class="sidebar-mini-hide">{{ _lang.logout }}</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>



        <!--Header-->
        <header id="header-navbar" class="">

            <ul class="nav-header pull-right">
                <li class="dropdown active active-blue">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" type="button">
                        <i class="flaticon-cross"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a tabindex="-1" href="#/tasks">
                                <i class="flaticon-interface-1"></i> {{ _lang.task_new_task }}
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a tabindex="-1" href="#/events/new">
                                <i class="flaticon-business"></i> {{ _lang.task_new_event }}
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="hidden-md hidden-lg active active-dark">
                    <a data-toggle="layout" data-action="sidebar_toggle" type="button" href="javascript:;" class="hidden-md hidden-lg">
                        <i class="flaticon-bars"></i>
                    </a>
                </li>
            </ul>
        </header>
        <!--/header-->



        <!-- Main Container -->
        <main id="main-container">
            <div class="container content">
                    <!-- <div class="view-slide-in" ng-view></div> -->
                    <div class="view-slide-in" ui-view></div>
            </div>
        </main>

        <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-white clearfix text-center">
            <div class="text-dark">
                &copy; 2016 Taskimi. With <i class="flaticon-shapes-1"></i> from Sydney
            </div>
        </footer>
    </div>

    <script src="/assets/js/core/jquery.min.js"></script>
    <script src="/assets/js/core/bootstrap.min.js"></script>
    <script src="/assets/js/app.min.js"></script>



    <script>
    $('.form_options').hide();
    $('.show_form').on('click', function(){
        $('.form_options').slideToggle('fast');
    });
    var _taskimy = <?php echo $lines; ?>;
    var _user = <?php echo $user; ?>;

    $(function () {
        App.initHelpers(['datepicker']);
    });

    $(window).load(function () {
        $(".loader .fading-line").fadeOut();
        $(".loader").fadeOut("slow");
    });

    </script>
</body>
</html>
