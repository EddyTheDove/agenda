<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Taskimy - My Account</title>
    @include('front.includes.head')
</head>
<body>
    <div id="page-container" class="sidebar-l sidebar-mini sidebar-o header-navbar-fixed">

        @include('front.includes.aside')
        @include('front.includes.header')

        <div class="backdrop"></div>
        <div class="fab child" data-subitem="1"><span><i class="flaticon-inteface-1"></i></span></div>
        <div class="fab child" data-subitem="2"><span>B</span></div>
        <div class="fab child" data-subitem="3"><span>A</span></div>
        <div class="fab" id="masterfab"><span>+</span></div>

        <!-- Main Container -->
        <main id="main-container">

            <div class="container">
                <div class="content">
                    <div class="row">
                        <!--Profile-->
                        <div class="col-sm-4">
                            <div class="block block-bordered">
                                <div class="block-header">
                                    <div class=" text-center">
                                        <h4 class="h5"><i class="si si-user"></i> {{ trans('app.account') }}</h4>
                                    </div>
                                </div>
                                <form class="" action="" method="post">
                                    {!! csrf_field() !!}
                                    <div class="block-content mt-20">


                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group" style="margin-bottom:10px !important;">
                                                    <div class="form-material floating ">
                                                        <input class="form-control" type="text" id="firstname" name="firstname" value="{{ $user->firstname }}">
                                                        <label for="firstname">{{ trans('messages.enter_firstname') }}</label>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="form-material floating">
                                                        <input class="form-control" type="text" id="lastname" name="lastname" value="{{ $user->lastname }}">
                                                        <label for="lastname">{{ trans('messages.enter_lastname') }}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="mt-20">
                                            <div class="form-group">
                                                <div class="form-material floating">
                                                    <input class="form-control" type="text" id="email" name="email" value="{{ $user->email }}">
                                                    <label for="email">{{ trans('messages.enter_email') }}</label>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="text-right mb-20">
                                            <button type="submit"  class="btn btn-purple btn-lg">
                                                <i class="si si-refresh"></i> {{ trans('app.btn_save') }}
                                            </button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </form>
                            </div>
                        </div>


                        <!--Password-->
                        <div class="col-sm-4">
                            <div class="block block-bordered mb-20">
                                <div class="block-header text-center">
                                    <h4 class="h5"><i class="si si-lock"></i> Update password</h4>
                                </div>

                                <div class="block-content">
                                    <form class="" action="/password" method="post">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="form-material floating ">
                                                        <input class="form-control" type="password" id="password" name="password">
                                                        <label for="password">{{ trans('messages.profile_current_password') }}</label>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="form-material floating">
                                                        <input class="form-control" type="password" id="newpassword" name="newpassword">
                                                        <label for="newpassword">{{ trans('messages.profile_new_password') }}</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="form-material floating">
                                                        <input class="form-control" type="password" id="newpasswordconfirmation" name="password_confirmation">
                                                        <label for="newpasswordconfirmation">{{ trans('messages.confirm_password') }}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="text-right mb-20">
                                            <button type="submit" name="button" class="btn btn-lg btn-purple">
                                                <i class="si si-refresh"></i> Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>

                        <!--Settings-->
                        <div class="col-sm-4">
                            <div class="block block-bordered mb-20">
                                <div class="block-header text-center">
                                    <h4 class="h5"><i class="si si-settings"></i> Account settings</h4>
                                </div>

                                <div class="block-content">
                                    <form class="" action="/password" method="post">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="form-material floating ">
                                                        <input class="form-control" type="password" id="password" name="password">
                                                        <label for="password">{{ trans('messages.profile_current_password') }}</label>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="form-material floating">
                                                        <input class="form-control" type="password" id="newpassword" name="newpassword">
                                                        <label for="newpassword">{{ trans('messages.profile_new_password') }}</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="form-material floating">
                                                        <input class="form-control" type="password" id="newpasswordconfirmation" name="password_confirmation">
                                                        <label for="newpasswordconfirmation">{{ trans('messages.confirm_password') }}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="text-right mb-20">
                                            <button type="submit" name="button" class="btn btn-lg btn-purple">
                                                <i class="si si-refresh"></i> Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="mt-40 text-right">
                        <div class="h5">
                            {{ trans('app.taskimy_profile_description') }}
                        </div>
                        <img src="/assets/img/taskimy-help.png" width="150" alt=""  class=""/>
                    </div>
                </div>
            </div>


        </main>

        <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-white clearfix text-center">
            <div class="text-dark">
                &copy; 2016 Taskimy. With <i class="flaticon-shapes-1"></i> from Sydney
            </div>
        </footer>
    </div>



    <script src="/assets/js/core/jquery.min.js"></script>
    <script src="/assets/js/core/bootstrap.min.js"></script>
    <script src="/assets/js/core/jquery.slimscroll.min.js"></script>
    <script src="/assets/js/core/jquery.scrollLock.min.js"></script>
    <script src="/assets/js/core/jquery.appear.min.js"></script>
    <script src="/assets/js/core/jquery.countTo.min.js"></script>
    <script src="/assets/js/core/jquery.placeholder.min.js"></script>
    <script src="/assets/js/core/js.cookie.min.js"></script>
    <script src="/assets/js/app.js"></script>

    <script src="/assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script>
    $('.form_options').hide();
    $('.show_form').on('click', function(){
        $('.form_options').slideToggle('fast');
    });

    $(function () {
        App.initHelpers(['datepicker']);
    });

    $(function(){
    	$(".fab,.backdrop").click(function(){
    		if($(".backdrop").is(":visible")){
    			$(".backdrop").fadeOut(125);
    			$(".fab.child")
    				.stop()
    				.animate({
    					bottom	: $("#masterfab").css("bottom"),
    					opacity	: 0
    				},125,function(){
    					$(this).hide();
    				});
    		}else{
    			$(".backdrop").fadeIn(125);
    			$(".fab.child").each(function(){
    				$(this)
    					.stop()
    					.show()
    					.animate({
    						bottom	: (parseInt($("#masterfab").css("bottom")) + parseInt($("#masterfab").outerHeight()) + 70 * $(this).data("subitem") - $(".fab.child").outerHeight()) + "px",
    						opacity	: 1
    					},125);
    			});
    		}
    	});
    });
    </script>
</body>
</html>
